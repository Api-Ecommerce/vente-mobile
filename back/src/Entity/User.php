<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="productSale")
     */
    private $productSales;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="productPurchase")
     */
    private $productPurchases;



    public function __construct()
    {
        $this->productSales = new ArrayCollection();
        $this->productPurchases = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsername() : ? string
    {
        return $this->username;
    }

    public function setUsername(string $username) : self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail() : ? string
    {
        return $this->email;
    }

    public function setEmail(string $email) : self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword() : ? string
    {
        return $this->password;
    }

    public function setPassword(string $password) : self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProductSales() : Collection
    {
        return $this->productSales;
    }

    public function addProductSale(Product $productSale) : self
    {
        if (!$this->productSales->contains($productSale)) {
            $this->productSales[] = $productSale;
            $productSale->setProductSale($this);
        }

        return $this;
    }

    public function removeProductSale(Product $productSale) : self
    {
        if ($this->productSales->contains($productSale)) {
            $this->productSales->removeElement($productSale);
            // set the owning side to null (unless already changed)
            if ($productSale->getProductSale() === $this) {
                $productSale->setProductSale(null);
            }
        }

        return $this;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function getSalt()
    {

    }
    public function eraseCredentials()
    {

    }

    /**
     * @return Collection|Product[]
     */
    public function getProductPurchases(): Collection
    {
        return $this->productPurchases;
    }

    public function addProductPurchase(Product $productPurchase): self
    {
        if (!$this->productPurchases->contains($productPurchase)) {
            $this->productPurchases[] = $productPurchase;
            $productPurchase->setProductPurchase($this);
        }

        return $this;
    }

    public function removeProductPurchase(Product $productPurchase): self
    {
        if ($this->productPurchases->contains($productPurchase)) {
            $this->productPurchases->removeElement($productPurchase);
            // set the owning side to null (unless already changed)
            if ($productPurchase->getProductPurchase() === $this) {
                $productPurchase->setProductPurchase(null);
            }
        }

        return $this;
    }

}
