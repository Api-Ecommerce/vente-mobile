<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Category;
use App\Entity\Product;


/**
 * @Route("/api/category", name="api_category")
 */
class ApiCategoryController extends Controller
{
    /**
     * @Route("/", methods="POST")
     */
    public function addCategory(Request $req)
    {

        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();
        $category = $serializer->deserialize(
            $req->getContent(),
            Category::class,
            "json"
        );
        $manager->persist($category);
        $manager->flush();

        $json = $serializer->serialize($category, "json");

        return JsonResponse::fromJsonString($json, 201);
    }

    /**
     * @Route("/", methods="GET")
     */
    public function findAllCat()
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($category, 'json')
        );
    }

    /**
     * @Route("/catChoise/{id}", methods="GET")
     */
    public function findByCategory(Category $category)
    {
        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($category, 'json')
        );
    }

    /**
     * @Route("/{id}", methods="DELETE")
     */
    public function delete(Category $category)
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($category);
        $manager->flush();

        return new JsonResponse([], 204);
    }

    /**
     * @Route("/{id}", methods="PUT")
     */
    public function update(Category $category, Request $req)
    {
        $manager = $this->getDoctrine()->getManager();

        $serializer = $this->get('jms_serializer');

        $body = $serializer->deserialize(
            $req->getContent(),
            Category::class,
            "json"
        );

        $category->setName($body->getName());


        $manager->flush();

        $json = $serializer->serialize($category, "json");

        return JsonResponse::fromJsonString($json, 201);
    }

}
