<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Product;
use App\Entity\Category;
use App\Form\CategoryType;
use App\Form\ProductType;




/**
 * @Route("/api/product", name="api_product")
 */
class ApiProductController extends Controller
{
    /**
     * @Route("/", methods="POST")
     */
    public function addProduct(Request $req) {

        $user = $this->getUser();

        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();


        $form = $this->createForm(ProductType::class);
        $form->submit(json_decode($req->getContent(), true));


        $product = $form->getData();
        //rajoute le produit dans le tableau du user(productSale)
        $user->addProductSale($product);
        $manager->persist($user);
        $manager->persist($product);
        $manager->flush();

        $json = $serializer->serialize($product, "json");

        return JsonResponse::fromJsonString($json, 201);
    }


    /**
     * @Route("/", methods="GET")
     */
    public function findAllProduct()
    {
        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findAll();

        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($product, 'json')
        );
    }

    /**
     * @Route("/single/{product}", methods="GET")
     */
    public function findById(Product $product)
    {
        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($product, 'json')
        );
    }

    /**
     * @Route("/productUserSale" ,methods="GET")
     */
    public function findByUser(){
        $serializer = $this->get('jms_serializer');

        $user = $this->getUser();

            return JsonResponse::fromJsonString(
                $serializer->serialize($user->getProductSales(), 'json')
            );

    }

    /**
     * @Route("/catProduct/{category}", methods="GET")
     */
    public function findByCat(Category $category){
        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($category->getProducts(), 'json')
        );
    }

    /**
     * @Route("/{id}", methods="DELETE")
     */
    public function delete(Product $product)
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($product);
        $manager->flush();

        return new JsonResponse([], 204);
    }

    /**
     * @Route("/{id}", methods="PUT")
     */
    public function update(Product $product, Request $req)
    {
        $manager = $this->getDoctrine()->getManager();

        $serializer = $this->get('jms_serializer');

        $body = $serializer->deserialize(
            $req->getContent(),
            Product::class,
            "json"
        );

        $product->setName($body->getName());


        $manager->flush();

        $json = $serializer->serialize($product, "json");

        return JsonResponse::fromJsonString($json, 201);
    }
}


