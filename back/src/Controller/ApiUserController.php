<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Product;

/**
 * @Route("/api/user", name="api_user")
 */
class ApiUserController extends Controller
{
    /**
     * @Route("/", methods="POST")
     */
    public function addUser(
        Request $req,
        UserPasswordEncoderInterface $encoder
    ) {

        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();
        $user = $serializer->deserialize(
            $req->getContent(),
            User::class,
            "json"
        );
        $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
        
        $manager->persist($user);
        $manager->flush();

        $json = $serializer->serialize($user, "json");

        return JsonResponse::fromJsonString($json, 201);
    }

    /**
     * @Route("/", methods="GET")
     */
    public function findAll()
    {
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();

        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($users, 'json')
        );
    }

    /**
     * @Route("/user", methods="GET")
     */
    public function findByUser()
    {
        //On va chercher le user connecter
        $user = $this->getUser();

        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($user, 'json')
        );
    }

    /**
     * @Route("/{id}", methods="DELETE")
     */
    public function delete(User $user)
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($user);
        $manager->flush();

        return new JsonResponse([], 204);
    }

    /**
     * @Route("/{id}", methods="PUT")
     */
    public function update(User $user, Request $req,  UserPasswordEncoderInterface $encoder)
    {
        $manager = $this->getDoctrine()->getManager();

        $serializer = $this->get('jms_serializer');

        $body = $serializer->deserialize(
            $req->getContent(),
            User::class,
            "json"
        );

        $user->setUsername($body->getUsername());//prend la donnée avec le set qu'on lui envoie avec le get
        $user->setEmail($body->getEmail());
        $user->setPassword($encoder->encodePassword($user, $body->getPassword()));

        $manager->flush();

        $json = $serializer->serialize($user, "json");

        return JsonResponse::fromJsonString($json, 201);
    }

    /**
     * @Route("/getBySale", methods="GET")
     */
    public function getBySale(){

        $user = $this->getUser()->getId();

        $serializer = $this->get('jms_serializer');

        $repo = $this->getDoctrine()->getRepository(Product::class);

        $json = $this->serializer->serialize($repo->findBy(array('productSale' => $user)), "json");
        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/getByPurchase", methods="GET")
     */
    public function getByPurchase(){

        $user = $this->getUser()->getId();

        $serializer = $this->get('jms_serializer');

        $repo = $this->getDoctrine()->getRepository(Product::class);

        $json = $this->serializer->serialize($repo->findBy(array('productPurchase' => $user)), "json");
        return JsonResponse::fromJsonString($json);

    }


}
