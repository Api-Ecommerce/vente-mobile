"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var authentification_service_1 = require("../service/authentification.service");
var category_service_1 = require("../service/category.service");
var CategoryChoixComponent = /** @class */ (function () {
    function CategoryChoixComponent(userLog, service) {
        this.userLog = userLog;
        this.service = service;
        this.category = [];
        this.tabImg = ["", "~/assets/img/6.png", "~/assets/img/3.png", "~/assets/img/5.png", "~/assets/img/2.png", "~/assets/img/4.png", "~/assets/img/1.png"];
    }
    CategoryChoixComponent.prototype.ngOnInit = function () {
        var _this = this;
        //affiche les categories
        this.service.findAll().subscribe(function (data) { return _this.category = data; }, function (error) { return console.log(error); }, function () { return console.log(_this.category); });
    };
    CategoryChoixComponent.prototype.logged = function () {
        return this.userLog.isLogged();
    };
    CategoryChoixComponent.prototype.logout = function () {
        this.userLog.logout();
    };
    CategoryChoixComponent = __decorate([
        core_1.Component({
            selector: 'ns-category-choix',
            templateUrl: './category-choix.component.html',
            styleUrls: ['./category-choix.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [authentification_service_1.AuthentificationService, category_service_1.CategoryService])
    ], CategoryChoixComponent);
    return CategoryChoixComponent;
}());
exports.CategoryChoixComponent = CategoryChoixComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2F0ZWdvcnktY2hvaXguY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY2F0ZWdvcnktY2hvaXguY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELGdGQUE4RTtBQUM5RSxnRUFBOEQ7QUFTOUQ7SUFPRSxnQ0FBb0IsT0FBK0IsRUFBVSxPQUF1QjtRQUFoRSxZQUFPLEdBQVAsT0FBTyxDQUF3QjtRQUFVLFlBQU8sR0FBUCxPQUFPLENBQWdCO1FBTHBGLGFBQVEsR0FBYyxFQUFFLENBQUM7UUFFbEIsV0FBTSxHQUFrQixDQUFDLEVBQUUsRUFBQyxvQkFBb0IsRUFBQyxvQkFBb0IsRUFBQyxvQkFBb0IsRUFBQyxvQkFBb0IsRUFBQyxvQkFBb0IsRUFBQyxvQkFBb0IsQ0FBQyxDQUFDO0lBRzFFLENBQUM7SUFFekYseUNBQVEsR0FBUjtRQUFBLGlCQUtDO1FBSEMsd0JBQXdCO1FBQ3hCLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsU0FBUyxDQUFFLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLEVBQXBCLENBQW9CLEVBQUUsVUFBQyxLQUFLLElBQUssT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFsQixDQUFrQixFQUM3RixjQUFNLE9BQUEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLEVBQTFCLENBQTBCLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBR0QsdUNBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2pDLENBQUM7SUFHRCx1Q0FBTSxHQUFOO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBeEJVLHNCQUFzQjtRQU5sQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixXQUFXLEVBQUUsaUNBQWlDO1lBQzlDLFNBQVMsRUFBRSxDQUFDLGdDQUFnQyxDQUFDO1lBQzdDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUNwQixDQUFDO3lDQVE0QixrREFBdUIsRUFBa0Isa0NBQWU7T0FQekUsc0JBQXNCLENBNEJsQztJQUFELDZCQUFDO0NBQUEsQUE1QkQsSUE0QkM7QUE1Qlksd0RBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEF1dGhlbnRpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9hdXRoZW50aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgQ2F0ZWdvcnlTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9jYXRlZ29yeS5zZXJ2aWNlJztcbmltcG9ydCB7IENhdGVnb3J5IH0gZnJvbSAnLi4vZW50aXR5L2NhdGVnb3J5JztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbnMtY2F0ZWdvcnktY2hvaXgnLFxuICB0ZW1wbGF0ZVVybDogJy4vY2F0ZWdvcnktY2hvaXguY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9jYXRlZ29yeS1jaG9peC5jb21wb25lbnQuY3NzJ10sXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG59KVxuZXhwb3J0IGNsYXNzIENhdGVnb3J5Q2hvaXhDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNhdGVnb3J5OkNhdGVnb3J5W10gPSBbXTtcblxuICBwdWJsaWMgdGFiSW1nOiBBcnJheTxzdHJpbmc+ID0gW1wiXCIsXCJ+L2Fzc2V0cy9pbWcvNi5wbmdcIixcIn4vYXNzZXRzL2ltZy8zLnBuZ1wiLFwifi9hc3NldHMvaW1nLzUucG5nXCIsXCJ+L2Fzc2V0cy9pbWcvMi5wbmdcIixcIn4vYXNzZXRzL2ltZy80LnBuZ1wiLFwifi9hc3NldHMvaW1nLzEucG5nXCJdO1xuICBcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHVzZXJMb2c6QXV0aGVudGlmaWNhdGlvblNlcnZpY2UsIHByaXZhdGUgc2VydmljZTpDYXRlZ29yeVNlcnZpY2UpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuXG4gICAgLy9hZmZpY2hlIGxlcyBjYXRlZ29yaWVzXG4gICAgdGhpcy5zZXJ2aWNlLmZpbmRBbGwoKS5zdWJzY3JpYmUoIGRhdGEgPT4gdGhpcy5jYXRlZ29yeSA9IGRhdGEsIChlcnJvcikgPT4gY29uc29sZS5sb2coZXJyb3IpLFxuICAgICgpID0+IGNvbnNvbGUubG9nKHRoaXMuY2F0ZWdvcnkpKTtcbiAgfVxuXG5cbiAgbG9nZ2VkKCkge1xuICAgIHJldHVybiB0aGlzLnVzZXJMb2cuaXNMb2dnZWQoKTtcbiAgfVxuXG5cbiAgbG9nb3V0KCl7XG4gICAgdGhpcy51c2VyTG9nLmxvZ291dCgpO1xuICB9XG5cblxuXG59XG4iXX0=