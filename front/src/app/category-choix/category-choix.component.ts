import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from '../service/authentification.service';
import { CategoryService } from '../service/category.service';
import { Category } from '../entity/category';

@Component({
  selector: 'ns-category-choix',
  templateUrl: './category-choix.component.html',
  styleUrls: ['./category-choix.component.css'],
  moduleId: module.id,
})
export class CategoryChoixComponent implements OnInit {

  category:Category[] = [];

  public tabImg: Array<string> = ["","~/assets/img/6.png","~/assets/img/3.png","~/assets/img/5.png","~/assets/img/2.png","~/assets/img/4.png","~/assets/img/1.png"];
  

  constructor(private userLog:AuthentificationService, private service:CategoryService) { }

  ngOnInit() {

    //affiche les categories
    this.service.findAll().subscribe( data => this.category = data, (error) => console.log(error),
    () => console.log(this.category));
  }


  logged() {
    return this.userLog.isLogged();
  }


  logout(){
    this.userLog.logout();
  }



}
