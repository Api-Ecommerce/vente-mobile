import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/product.service';
import { Product } from '../entity/product';
import { Category } from '../entity/category';
import { CategoryService } from '../service/category.service';
import { AuthentificationService } from '../service/authentification.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'ns-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  moduleId: module.id,
})
export class HomeComponent implements OnInit {

  products:Product[] = [];
  categorys:Category[] = [];

  constructor(private serviceProduct:ProductService, private serviceCat:CategoryService ,private userLog:AuthentificationService) { }

  ngOnInit() {
    //affiche tous mes produits
    this.serviceProduct.findAll().subscribe( data => this.products = data );

    //affiche toute mes category
    this.serviceCat.findAll().subscribe( data => this.categorys = data);

  }

  logged() {
    return this.userLog.isLogged();
  }


  logout(){
    this.userLog.logout();
  }

  

}
