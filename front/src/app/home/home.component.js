"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var product_service_1 = require("../service/product.service");
var category_service_1 = require("../service/category.service");
var authentification_service_1 = require("../service/authentification.service");
var HomeComponent = /** @class */ (function () {
    function HomeComponent(serviceProduct, serviceCat, userLog) {
        this.serviceProduct = serviceProduct;
        this.serviceCat = serviceCat;
        this.userLog = userLog;
        this.products = [];
        this.categorys = [];
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        //affiche tous mes produits
        this.serviceProduct.findAll().subscribe(function (data) { return _this.products = data; });
        //affiche toute mes category
        this.serviceCat.findAll().subscribe(function (data) { return _this.categorys = data; });
    };
    HomeComponent.prototype.logged = function () {
        return this.userLog.isLogged();
    };
    HomeComponent.prototype.logout = function () {
        this.userLog.logout();
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'ns-home',
            templateUrl: './home.component.html',
            styleUrls: ['./home.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [product_service_1.ProductService, category_service_1.CategoryService, authentification_service_1.AuthentificationService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCw4REFBNEQ7QUFHNUQsZ0VBQThEO0FBQzlELGdGQUE4RTtBQVU5RTtJQUtFLHVCQUFvQixjQUE2QixFQUFVLFVBQTBCLEVBQVUsT0FBK0I7UUFBMUcsbUJBQWMsR0FBZCxjQUFjLENBQWU7UUFBVSxlQUFVLEdBQVYsVUFBVSxDQUFnQjtRQUFVLFlBQU8sR0FBUCxPQUFPLENBQXdCO1FBSDlILGFBQVEsR0FBYSxFQUFFLENBQUM7UUFDeEIsY0FBUyxHQUFjLEVBQUUsQ0FBQztJQUV3RyxDQUFDO0lBRW5JLGdDQUFRLEdBQVI7UUFBQSxpQkFPQztRQU5DLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDLFNBQVMsQ0FBRSxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxFQUFwQixDQUFvQixDQUFFLENBQUM7UUFFeEUsNEJBQTRCO1FBQzVCLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLENBQUMsU0FBUyxDQUFFLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLEVBQXJCLENBQXFCLENBQUMsQ0FBQztJQUV0RSxDQUFDO0lBRUQsOEJBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2pDLENBQUM7SUFHRCw4QkFBTSxHQUFOO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBdkJVLGFBQWE7UUFOekIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxTQUFTO1lBQ25CLFdBQVcsRUFBRSx1QkFBdUI7WUFDcEMsU0FBUyxFQUFFLENBQUMsc0JBQXNCLENBQUM7WUFDbkMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1NBQ3BCLENBQUM7eUNBTW1DLGdDQUFjLEVBQXFCLGtDQUFlLEVBQWtCLGtEQUF1QjtPQUxuSCxhQUFhLENBMkJ6QjtJQUFELG9CQUFDO0NBQUEsQUEzQkQsSUEyQkM7QUEzQlksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUHJvZHVjdFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlL3Byb2R1Y3Quc2VydmljZSc7XG5pbXBvcnQgeyBQcm9kdWN0IH0gZnJvbSAnLi4vZW50aXR5L3Byb2R1Y3QnO1xuaW1wb3J0IHsgQ2F0ZWdvcnkgfSBmcm9tICcuLi9lbnRpdHkvY2F0ZWdvcnknO1xuaW1wb3J0IHsgQ2F0ZWdvcnlTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9jYXRlZ29yeS5zZXJ2aWNlJztcbmltcG9ydCB7IEF1dGhlbnRpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9hdXRoZW50aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25zLWhvbWUnLFxuICB0ZW1wbGF0ZVVybDogJy4vaG9tZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2hvbWUuY29tcG9uZW50LmNzcyddLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxufSlcbmV4cG9ydCBjbGFzcyBIb21lQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBwcm9kdWN0czpQcm9kdWN0W10gPSBbXTtcbiAgY2F0ZWdvcnlzOkNhdGVnb3J5W10gPSBbXTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNlcnZpY2VQcm9kdWN0OlByb2R1Y3RTZXJ2aWNlLCBwcml2YXRlIHNlcnZpY2VDYXQ6Q2F0ZWdvcnlTZXJ2aWNlICxwcml2YXRlIHVzZXJMb2c6QXV0aGVudGlmaWNhdGlvblNlcnZpY2UpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIC8vYWZmaWNoZSB0b3VzIG1lcyBwcm9kdWl0c1xuICAgIHRoaXMuc2VydmljZVByb2R1Y3QuZmluZEFsbCgpLnN1YnNjcmliZSggZGF0YSA9PiB0aGlzLnByb2R1Y3RzID0gZGF0YSApO1xuXG4gICAgLy9hZmZpY2hlIHRvdXRlIG1lcyBjYXRlZ29yeVxuICAgIHRoaXMuc2VydmljZUNhdC5maW5kQWxsKCkuc3Vic2NyaWJlKCBkYXRhID0+IHRoaXMuY2F0ZWdvcnlzID0gZGF0YSk7XG5cbiAgfVxuXG4gIGxvZ2dlZCgpIHtcbiAgICByZXR1cm4gdGhpcy51c2VyTG9nLmlzTG9nZ2VkKCk7XG4gIH1cblxuXG4gIGxvZ291dCgpe1xuICAgIHRoaXMudXNlckxvZy5sb2dvdXQoKTtcbiAgfVxuXG4gIFxuXG59XG4iXX0=