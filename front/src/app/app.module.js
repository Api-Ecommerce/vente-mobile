"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var forms_1 = require("nativescript-angular/forms");
var http_client_1 = require("nativescript-angular/http-client");
var app_routing_1 = require("./app.routing");
var app_component_1 = require("./app.component");
var http_1 = require("@angular/common/http");
var login_component_1 = require("./login/login.component");
var product_add_component_1 = require("./product-add/product-add.component");
var home_component_1 = require("./home/home.component");
var forms_2 = require("@angular/forms");
var gestion_product_component_1 = require("./gestion-product/gestion-product.component");
var interceptor_service_1 = require("./service/interceptor/interceptor.service");
var single_product_component_1 = require("./single-product/single-product.component");
var category_choix_component_1 = require("./category-choix/category-choix.component");
var categor_choisi_component_1 = require("./categor-choisi/categor-choisi.component");
// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";
// Uncomment and add to NgModule imports if you need to use the HttpClient wrapper
// import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
var AppModule = /** @class */ (function () {
    /*
    Pass your application module to the bootstrapModule function located in main.ts to start your app
    */
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            bootstrap: [
                app_component_1.AppComponent
            ],
            imports: [
                nativescript_module_1.NativeScriptModule,
                app_routing_1.AppRoutingModule,
                forms_1.NativeScriptFormsModule,
                http_client_1.NativeScriptHttpClientModule,
                forms_2.FormsModule,
            ],
            declarations: [
                app_component_1.AppComponent,
                login_component_1.LoginComponent,
                product_add_component_1.ProductAddComponent,
                home_component_1.HomeComponent,
                gestion_product_component_1.GestionProductComponent,
                single_product_component_1.SingleProductComponent,
                category_choix_component_1.CategoryChoixComponent,
                categor_choisi_component_1.CategorChoisiComponent
            ],
            providers: [
                {
                    provide: http_1.HTTP_INTERCEPTORS,
                    useClass: interceptor_service_1.InterceptorService,
                    multi: true,
                },
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        })
        /*
        Pass your application module to the bootstrapModule function located in main.ts to start your app
        */
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFDM0QsZ0ZBQThFO0FBQzlFLG9EQUFxRTtBQUNyRSxnRUFBZ0Y7QUFFaEYsNkNBQWlEO0FBQ2pELGlEQUErQztBQUUvQyw2Q0FBeUQ7QUFDekQsMkRBQXlEO0FBQ3pELDZFQUEwRTtBQUUxRSx3REFBc0Q7QUFDdEQsd0NBQTZDO0FBQzdDLHlGQUFzRjtBQUN0RixpRkFBK0U7QUFDL0Usc0ZBQW1GO0FBQ25GLHNGQUFtRjtBQUNuRixzRkFBbUY7QUFHbkYsMkVBQTJFO0FBQzNFLHdFQUF3RTtBQUV4RSxrRkFBa0Y7QUFDbEYsbUZBQW1GO0FBZ0RuRjtJQUhBOztNQUVFO0lBQ0Y7SUFBeUIsQ0FBQztJQUFiLFNBQVM7UUE5Q3JCLGVBQVEsQ0FBQztZQUVOLFNBQVMsRUFBRTtnQkFDUCw0QkFBWTthQUNmO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLHdDQUFrQjtnQkFDbEIsOEJBQWdCO2dCQUNoQiwrQkFBdUI7Z0JBQ3ZCLDBDQUE0QjtnQkFDNUIsbUJBQVc7YUFDZDtZQUNELFlBQVksRUFBRTtnQkFFZCw0QkFBWTtnQkFFWixnQ0FBYztnQkFFZCwyQ0FBbUI7Z0JBRW5CLDhCQUFhO2dCQUViLG1EQUF1QjtnQkFFdkIsaURBQXNCO2dCQUV0QixpREFBc0I7Z0JBRXRCLGlEQUFzQjthQUV6QjtZQUNHLFNBQVMsRUFBRTtnQkFDUDtvQkFDSSxPQUFPLEVBQUUsd0JBQWlCO29CQUMxQixRQUFRLEVBQUUsd0NBQWtCO29CQUM1QixLQUFLLEVBQUUsSUFBSTtpQkFFWjthQUNOO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLHVCQUFnQjthQUNuQjtTQUNKLENBQUM7UUFDRjs7VUFFRTtPQUNXLFNBQVMsQ0FBSTtJQUFELGdCQUFDO0NBQUEsQUFBMUIsSUFBMEI7QUFBYiw4QkFBUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9uYXRpdmVzY3JpcHQubW9kdWxlXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0SHR0cENsaWVudE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9odHRwLWNsaWVudFwiO1xuXG5pbXBvcnQgeyBBcHBSb3V0aW5nTW9kdWxlIH0gZnJvbSBcIi4vYXBwLnJvdXRpbmdcIjtcbmltcG9ydCB7IEFwcENvbXBvbmVudCB9IGZyb20gXCIuL2FwcC5jb21wb25lbnRcIjtcblxuaW1wb3J0IHsgSFRUUF9JTlRFUkNFUFRPUlMgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uL2h0dHBcIjtcbmltcG9ydCB7IExvZ2luQ29tcG9uZW50IH0gZnJvbSAnLi9sb2dpbi9sb2dpbi5jb21wb25lbnQnO1xuaW1wb3J0IHsgUHJvZHVjdEFkZENvbXBvbmVudCB9IGZyb20gJy4vcHJvZHVjdC1hZGQvcHJvZHVjdC1hZGQuY29tcG9uZW50JztcblxuaW1wb3J0IHsgSG9tZUNvbXBvbmVudCB9IGZyb20gJy4vaG9tZS9ob21lLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBGb3Jtc01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHsgR2VzdGlvblByb2R1Y3RDb21wb25lbnQgfSBmcm9tICcuL2dlc3Rpb24tcHJvZHVjdC9nZXN0aW9uLXByb2R1Y3QuY29tcG9uZW50JztcbmltcG9ydCB7IEludGVyY2VwdG9yU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2UvaW50ZXJjZXB0b3IvaW50ZXJjZXB0b3Iuc2VydmljZVwiO1xuaW1wb3J0IHsgU2luZ2xlUHJvZHVjdENvbXBvbmVudCB9IGZyb20gJy4vc2luZ2xlLXByb2R1Y3Qvc2luZ2xlLXByb2R1Y3QuY29tcG9uZW50JztcbmltcG9ydCB7IENhdGVnb3J5Q2hvaXhDb21wb25lbnQgfSBmcm9tICcuL2NhdGVnb3J5LWNob2l4L2NhdGVnb3J5LWNob2l4LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDYXRlZ29yQ2hvaXNpQ29tcG9uZW50IH0gZnJvbSAnLi9jYXRlZ29yLWNob2lzaS9jYXRlZ29yLWNob2lzaS5jb21wb25lbnQnO1xuXG5cbi8vIFVuY29tbWVudCBhbmQgYWRkIHRvIE5nTW9kdWxlIGltcG9ydHMgaWYgeW91IG5lZWQgdG8gdXNlIHR3by13YXkgYmluZGluZ1xuLy8gaW1wb3J0IHsgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZm9ybXNcIjtcblxuLy8gVW5jb21tZW50IGFuZCBhZGQgdG8gTmdNb2R1bGUgaW1wb3J0cyBpZiB5b3UgbmVlZCB0byB1c2UgdGhlIEh0dHBDbGllbnQgd3JhcHBlclxuLy8gaW1wb3J0IHsgTmF0aXZlU2NyaXB0SHR0cENsaWVudE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9odHRwLWNsaWVudFwiO1xuXG5ATmdNb2R1bGUoe1xuXG4gICAgYm9vdHN0cmFwOiBbXG4gICAgICAgIEFwcENvbXBvbmVudFxuICAgIF0sXG4gICAgaW1wb3J0czogW1xuICAgICAgICBOYXRpdmVTY3JpcHRNb2R1bGUsXG4gICAgICAgIEFwcFJvdXRpbmdNb2R1bGUsXG4gICAgICAgIE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlLFxuICAgICAgICBOYXRpdmVTY3JpcHRIdHRwQ2xpZW50TW9kdWxlLFxuICAgICAgICBGb3Jtc01vZHVsZSxcbiAgICBdLFxuICAgIGRlY2xhcmF0aW9uczogW1xuICAgIFxuICAgIEFwcENvbXBvbmVudCxcblxuICAgIExvZ2luQ29tcG9uZW50LFxuXG4gICAgUHJvZHVjdEFkZENvbXBvbmVudCxcblxuICAgIEhvbWVDb21wb25lbnQsXG5cbiAgICBHZXN0aW9uUHJvZHVjdENvbXBvbmVudCxcblxuICAgIFNpbmdsZVByb2R1Y3RDb21wb25lbnQsXG5cbiAgICBDYXRlZ29yeUNob2l4Q29tcG9uZW50LFxuXG4gICAgQ2F0ZWdvckNob2lzaUNvbXBvbmVudFxuXG5dLFxuICAgIHByb3ZpZGVyczogW1xuICAgICAgICB7XG4gICAgICAgICAgICBwcm92aWRlOiBIVFRQX0lOVEVSQ0VQVE9SUyxcbiAgICAgICAgICAgIHVzZUNsYXNzOiBJbnRlcmNlcHRvclNlcnZpY2UsXG4gICAgICAgICAgICBtdWx0aTogdHJ1ZSxcbiAgICAgICAgICAgIFxuICAgICAgICAgIH0sXG4gICAgXSxcbiAgICBzY2hlbWFzOiBbXG4gICAgICAgIE5PX0VSUk9SU19TQ0hFTUFcbiAgICBdXG59KVxuLypcblBhc3MgeW91ciBhcHBsaWNhdGlvbiBtb2R1bGUgdG8gdGhlIGJvb3RzdHJhcE1vZHVsZSBmdW5jdGlvbiBsb2NhdGVkIGluIG1haW4udHMgdG8gc3RhcnQgeW91ciBhcHBcbiovXG5leHBvcnQgY2xhc3MgQXBwTW9kdWxlIHsgfVxuIl19