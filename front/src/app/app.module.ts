import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";

import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { LoginComponent } from './login/login.component';
import { ProductAddComponent } from './product-add/product-add.component';

import { HomeComponent } from './home/home.component';
import { FormsModule } from "@angular/forms";
import { GestionProductComponent } from './gestion-product/gestion-product.component';
import { InterceptorService } from "./service/interceptor/interceptor.service";
import { SingleProductComponent } from './single-product/single-product.component';
import { CategoryChoixComponent } from './category-choix/category-choix.component';
import { CategorChoisiComponent } from './categor-choisi/categor-choisi.component';


// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";

// Uncomment and add to NgModule imports if you need to use the HttpClient wrapper
// import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

@NgModule({

    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        FormsModule,
    ],
    declarations: [
    
    AppComponent,

    LoginComponent,

    ProductAddComponent,

    HomeComponent,

    GestionProductComponent,

    SingleProductComponent,

    CategoryChoixComponent,

    CategorChoisiComponent

],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: InterceptorService,
            multi: true,
            
          },
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
