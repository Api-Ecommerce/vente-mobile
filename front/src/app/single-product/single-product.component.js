"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var product_service_1 = require("../service/product.service");
var router_1 = require("nativescript-angular/router");
var operators_1 = require("rxjs/operators");
var environment_1 = require("~/environments/environment");
var authentification_service_1 = require("../service/authentification.service");
var SingleProductComponent = /** @class */ (function () {
    function SingleProductComponent(pageRoute, service, userLog) {
        this.pageRoute = pageRoute;
        this.service = service;
        this.userLog = userLog;
    }
    SingleProductComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.pageRoute.activatedRoute.pipe(operators_1.switchMap(function (activatedRoute) { return activatedRoute.params; }), operators_1.switchMap(function (params) { return _this.service.find(params['id']); })).subscribe(function (data) { return _this.product = data; }, function (error) { return console.log(error); });
    };
    Object.defineProperty(SingleProductComponent.prototype, "picture", {
        get: function () {
            return environment_1.environment.serverUrl + '/' + this.product.img;
        },
        enumerable: true,
        configurable: true
    });
    SingleProductComponent.prototype.logged = function () {
        return this.userLog.isLogged();
    };
    SingleProductComponent.prototype.logout = function () {
        this.userLog.logout();
    };
    SingleProductComponent = __decorate([
        core_1.Component({
            selector: 'ns-single-product',
            templateUrl: './single-product.component.html',
            styleUrls: ['./single-product.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [router_1.PageRoute, product_service_1.ProductService, authentification_service_1.AuthentificationService])
    ], SingleProductComponent);
    return SingleProductComponent;
}());
exports.SingleProductComponent = SingleProductComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2luZ2xlLXByb2R1Y3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2luZ2xlLXByb2R1Y3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBRWxELDhEQUE0RDtBQUM1RCxzREFBd0Q7QUFDeEQsNENBQTJDO0FBQzNDLDBEQUF5RDtBQUN6RCxnRkFBOEU7QUFROUU7SUFJRSxnQ0FBb0IsU0FBb0IsRUFBVSxPQUF1QixFQUFTLE9BQStCO1FBQTdGLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFBVSxZQUFPLEdBQVAsT0FBTyxDQUFnQjtRQUFTLFlBQU8sR0FBUCxPQUFPLENBQXdCO0lBQUksQ0FBQztJQUV0SCx5Q0FBUSxHQUFSO1FBQUEsaUJBU0M7UUFSQyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQ2hDLHFCQUFTLENBQUMsVUFBQSxjQUFjLElBQUksT0FBQSxjQUFjLENBQUMsTUFBTSxFQUFyQixDQUFxQixDQUFDLEVBQ2xELHFCQUFTLENBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBL0IsQ0FBK0IsQ0FBQyxDQUNyRCxDQUFDLFNBQVMsQ0FDVCxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxFQUFuQixDQUFtQixFQUMzQixVQUFBLEtBQUssSUFBSSxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQWxCLENBQWtCLENBQzVCLENBQUE7SUFFSCxDQUFDO0lBRUQsc0JBQUksMkNBQU87YUFBWDtZQUNFLE1BQU0sQ0FBQyx5QkFBVyxDQUFDLFNBQVMsR0FBQyxHQUFHLEdBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7UUFDcEQsQ0FBQzs7O09BQUE7SUFFRCx1Q0FBTSxHQUFOO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDakMsQ0FBQztJQUdELHVDQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUE1QlUsc0JBQXNCO1FBTmxDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLFdBQVcsRUFBRSxpQ0FBaUM7WUFDOUMsU0FBUyxFQUFFLENBQUMsZ0NBQWdDLENBQUM7WUFDN0MsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1NBQ3BCLENBQUM7eUNBSytCLGtCQUFTLEVBQW1CLGdDQUFjLEVBQWlCLGtEQUF1QjtPQUp0RyxzQkFBc0IsQ0FpQ2xDO0lBQUQsNkJBQUM7Q0FBQSxBQWpDRCxJQWlDQztBQWpDWSx3REFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUHJvZHVjdCB9IGZyb20gJy4uL2VudGl0eS9wcm9kdWN0JztcbmltcG9ydCB7IFByb2R1Y3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9wcm9kdWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgUGFnZVJvdXRlIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IHN3aXRjaE1hcCB9IGZyb20gXCJyeGpzL29wZXJhdG9yc1wiO1xuaW1wb3J0IHsgZW52aXJvbm1lbnQgfSBmcm9tICd+L2Vudmlyb25tZW50cy9lbnZpcm9ubWVudCc7XG5pbXBvcnQgeyBBdXRoZW50aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2UvYXV0aGVudGlmaWNhdGlvbi5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbnMtc2luZ2xlLXByb2R1Y3QnLFxuICB0ZW1wbGF0ZVVybDogJy4vc2luZ2xlLXByb2R1Y3QuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9zaW5nbGUtcHJvZHVjdC5jb21wb25lbnQuY3NzJ10sXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG59KVxuZXhwb3J0IGNsYXNzIFNpbmdsZVByb2R1Y3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIHByb2R1Y3Q6IFByb2R1Y3Q7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwYWdlUm91dGU6IFBhZ2VSb3V0ZSwgcHJpdmF0ZSBzZXJ2aWNlOiBQcm9kdWN0U2VydmljZSxwcml2YXRlIHVzZXJMb2c6QXV0aGVudGlmaWNhdGlvblNlcnZpY2UpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMucGFnZVJvdXRlLmFjdGl2YXRlZFJvdXRlLnBpcGUoXG4gICAgICBzd2l0Y2hNYXAoYWN0aXZhdGVkUm91dGUgPT4gYWN0aXZhdGVkUm91dGUucGFyYW1zKSxcbiAgICAgIHN3aXRjaE1hcChwYXJhbXMgPT4gdGhpcy5zZXJ2aWNlLmZpbmQocGFyYW1zWydpZCddKSlcbiAgICApLnN1YnNjcmliZShcbiAgICAgIGRhdGEgPT4gdGhpcy5wcm9kdWN0ID0gZGF0YSxcbiAgICAgIGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKVxuICAgIClcblxuICB9XG5cbiAgZ2V0IHBpY3R1cmUoKSB7XG4gICAgcmV0dXJuIGVudmlyb25tZW50LnNlcnZlclVybCsnLycrdGhpcy5wcm9kdWN0LmltZztcbiAgfVxuXG4gIGxvZ2dlZCgpIHtcbiAgICByZXR1cm4gdGhpcy51c2VyTG9nLmlzTG9nZ2VkKCk7XG4gIH1cblxuXG4gIGxvZ291dCgpe1xuICAgIHRoaXMudXNlckxvZy5sb2dvdXQoKTtcbiAgfVxuXG5cblxuXG59XG4iXX0=