import { Component, OnInit } from '@angular/core';
import { Product } from '../entity/product';
import { ProductService } from '../service/product.service';
import { PageRoute } from 'nativescript-angular/router';
import { switchMap } from "rxjs/operators";
import { environment } from '~/environments/environment';
import { AuthentificationService } from '../service/authentification.service';

@Component({
  selector: 'ns-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.css'],
  moduleId: module.id,
})
export class SingleProductComponent implements OnInit {

  product: Product;

  constructor(private pageRoute: PageRoute, private service: ProductService,private userLog:AuthentificationService) { }

  ngOnInit() {
    this.pageRoute.activatedRoute.pipe(
      switchMap(activatedRoute => activatedRoute.params),
      switchMap(params => this.service.find(params['id']))
    ).subscribe(
      data => this.product = data,
      error => console.log(error)
    )

  }

  get picture() {
    return environment.serverUrl+'/'+this.product.img;
  }

  logged() {
    return this.userLog.isLogged();
  }


  logout(){
    this.userLog.logout();
  }




}
