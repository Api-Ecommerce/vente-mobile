import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";


import { LoginComponent } from "./login/login.component";
import { ProductAddComponent } from "./product-add/product-add.component";
import { HomeComponent } from "./home/home.component";
import { GestionProductComponent } from "./gestion-product/gestion-product.component";
import { SingleProductComponent } from "./single-product/single-product.component";
import { CategoryChoixComponent } from "./category-choix/category-choix.component";
import { CategorChoisiComponent } from "./categor-choisi/categor-choisi.component";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", component: HomeComponent},
    { path: "login", component: LoginComponent},
    { path: "product-add", component:ProductAddComponent},
    { path: "gestion", component:GestionProductComponent},
    { path: "single/:id", component:SingleProductComponent},
    { path: "categoryChoix", component:CategoryChoixComponent },
    { path: "categoryChoisi/:id", component:CategorChoisiComponent }


];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }