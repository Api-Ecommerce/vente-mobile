"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var authentification_service_1 = require("../service/authentification.service");
var AutorisationGuard = /** @class */ (function () {
    function AutorisationGuard(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    AutorisationGuard.prototype.canActivate = function (next, state) {
        if (this.auth.isLogged()) {
            return true;
        }
        //On dit au router où nous rediriger si jamais on est pas loggé
        return false;
        // return this.auth.isLoggedIn();
        // return this.auth.user;
    };
    AutorisationGuard = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [authentification_service_1.AuthentificationService, router_1.Router])
    ], AutorisationGuard);
    return AutorisationGuard;
}());
exports.AutorisationGuard = AutorisationGuard;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b3Jpc2F0aW9uLmd1YXJkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXV0b3Jpc2F0aW9uLmd1YXJkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLDBDQUFxSDtBQUVySCxnRkFBOEU7QUFNOUU7SUFFRSwyQkFBb0IsSUFBNkIsRUFBVSxNQUFjO1FBQXJELFNBQUksR0FBSixJQUFJLENBQXlCO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBUTtJQUV6RSxDQUFDO0lBRUQsdUNBQVcsR0FBWCxVQUNFLElBQTRCLEVBQzVCLEtBQTBCO1FBRTFCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDZCxDQUFDO1FBQ0QsK0RBQStEO1FBQy9ELE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDYixpQ0FBaUM7UUFDakMseUJBQXlCO0lBQzNCLENBQUM7SUFqQlUsaUJBQWlCO1FBSDdCLGlCQUFVLENBQUM7WUFDVixVQUFVLEVBQUUsTUFBTTtTQUNuQixDQUFDO3lDQUcwQixrREFBdUIsRUFBa0IsZUFBTTtPQUY5RCxpQkFBaUIsQ0FrQjdCO0lBQUQsd0JBQUM7Q0FBQSxBQWxCRCxJQWtCQztBQWxCWSw4Q0FBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDYW5BY3RpdmF0ZSwgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUm91dGVyU3RhdGVTbmFwc2hvdCwgUm91dGVyLCBDYW5BY3RpdmF0ZUNoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEF1dGhlbnRpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9hdXRoZW50aWZpY2F0aW9uLnNlcnZpY2UnO1xuXG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIEF1dG9yaXNhdGlvbkd1YXJkIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUge1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgYXV0aDogQXV0aGVudGlmaWNhdGlvblNlcnZpY2UsIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHtcblxuICB9XG5cbiAgY2FuQWN0aXZhdGUoXG4gICAgbmV4dDogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcbiAgICBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IE9ic2VydmFibGU8Ym9vbGVhbj4gfCBQcm9taXNlPGJvb2xlYW4+IHwgYm9vbGVhbiB7XG5cbiAgICBpZiAodGhpcy5hdXRoLmlzTG9nZ2VkKCkpIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgICAvL09uIGRpdCBhdSByb3V0ZXIgb8O5IG5vdXMgcmVkaXJpZ2VyIHNpIGphbWFpcyBvbiBlc3QgcGFzIGxvZ2fDqVxuICAgIHJldHVybiBmYWxzZTtcbiAgICAvLyByZXR1cm4gdGhpcy5hdXRoLmlzTG9nZ2VkSW4oKTtcbiAgICAvLyByZXR1cm4gdGhpcy5hdXRoLnVzZXI7XG4gIH1cbn0iXX0=