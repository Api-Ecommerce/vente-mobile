import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthentificationService } from '../service/authentification.service';


@Injectable({
  providedIn: 'root'
})
export class AutorisationGuard implements CanActivate {

  constructor(private auth: AuthentificationService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (this.auth.isLogged()) {
      return true;
    }
    //On dit au router où nous rediriger si jamais on est pas loggé
    return false;
    // return this.auth.isLoggedIn();
    // return this.auth.user;
  }
}