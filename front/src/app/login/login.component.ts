import { Component, OnInit } from '@angular/core';

import { AuthentificationService } from '../service/authentification.service';
import { RouterExtensions } from 'nativescript-angular/router';
import { User } from '../entity/user';


@Component({
  selector: 'ns-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  moduleId: module.id,
})
export class LoginComponent implements OnInit {

  user:User = { username:'', email:'', password:''};
  confirm = '';
  signup ='';
  feedback = '';

  constructor(private authService : AuthentificationService, private routerExt : RouterExtensions) { }

  ngOnInit() {
  }

  register() {
    if(this.user.password === this.confirm) {

      this.authService.addUser(this.user)
        .subscribe(
          user => this.loginUser(),
          error => {
            console.log(error);
            
            this.feedback = 'Register error';
          } , () => console.log('ok')
        );
    } else {
      this.feedback = 'Password did not match.'
    }

  }

  loginUser() {
    this.authService.login(this.user.username, this.user.password)
    .subscribe(
      () => this.routerExt.backToPreviousPage(),
      error => {
         console.log(error);
         this.feedback = 'Login error.'
         }
    );
  }

  }

