"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var authentification_service_1 = require("../service/authentification.service");
var router_1 = require("nativescript-angular/router");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService, routerExt) {
        this.authService = authService;
        this.routerExt = routerExt;
        this.user = { username: '', email: '', password: '' };
        this.confirm = '';
        this.signup = '';
        this.feedback = '';
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.register = function () {
        var _this = this;
        if (this.user.password === this.confirm) {
            this.authService.addUser(this.user)
                .subscribe(function (user) { return _this.loginUser(); }, function (error) {
                console.log(error);
                _this.feedback = 'Register error';
            }, function () { return console.log('ok'); });
        }
        else {
            this.feedback = 'Password did not match.';
        }
    };
    LoginComponent.prototype.loginUser = function () {
        var _this = this;
        this.authService.login(this.user.username, this.user.password)
            .subscribe(function () { return _this.routerExt.backToPreviousPage(); }, function (error) {
            console.log(error);
            _this.feedback = 'Login error.';
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'ns-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [authentification_service_1.AuthentificationService, router_1.RouterExtensions])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBRWxELGdGQUE4RTtBQUM5RSxzREFBK0Q7QUFVL0Q7SUFPRSx3QkFBb0IsV0FBcUMsRUFBVSxTQUE0QjtRQUEzRSxnQkFBVyxHQUFYLFdBQVcsQ0FBMEI7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFtQjtRQUwvRixTQUFJLEdBQVEsRUFBRSxRQUFRLEVBQUMsRUFBRSxFQUFFLEtBQUssRUFBQyxFQUFFLEVBQUUsUUFBUSxFQUFDLEVBQUUsRUFBQyxDQUFDO1FBQ2xELFlBQU8sR0FBRyxFQUFFLENBQUM7UUFDYixXQUFNLEdBQUUsRUFBRSxDQUFDO1FBQ1gsYUFBUSxHQUFHLEVBQUUsQ0FBQztJQUVxRixDQUFDO0lBRXBHLGlDQUFRLEdBQVI7SUFDQSxDQUFDO0lBRUQsaUNBQVEsR0FBUjtRQUFBLGlCQWdCQztRQWZDLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBRXZDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7aUJBQ2hDLFNBQVMsQ0FDUixVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxTQUFTLEVBQUUsRUFBaEIsQ0FBZ0IsRUFDeEIsVUFBQSxLQUFLO2dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBRW5CLEtBQUksQ0FBQyxRQUFRLEdBQUcsZ0JBQWdCLENBQUM7WUFDbkMsQ0FBQyxFQUFHLGNBQU0sT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFqQixDQUFpQixDQUM1QixDQUFDO1FBQ04sQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLFFBQVEsR0FBRyx5QkFBeUIsQ0FBQTtRQUMzQyxDQUFDO0lBRUgsQ0FBQztJQUVELGtDQUFTLEdBQVQ7UUFBQSxpQkFTQztRQVJDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2FBQzdELFNBQVMsQ0FDUixjQUFNLE9BQUEsS0FBSSxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsRUFBRSxFQUFuQyxDQUFtQyxFQUN6QyxVQUFBLEtBQUs7WUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25CLEtBQUksQ0FBQyxRQUFRLEdBQUcsY0FBYyxDQUFBO1FBQzlCLENBQUMsQ0FDTCxDQUFDO0lBQ0osQ0FBQztJQXZDVSxjQUFjO1FBTjFCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsVUFBVTtZQUNwQixXQUFXLEVBQUUsd0JBQXdCO1lBQ3JDLFNBQVMsRUFBRSxDQUFDLHVCQUF1QixDQUFDO1lBQ3BDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUNwQixDQUFDO3lDQVFrQyxrREFBdUIsRUFBc0IseUJBQWdCO09BUHBGLGNBQWMsQ0F5Q3hCO0lBQUQscUJBQUM7Q0FBQSxBQXpDSCxJQXlDRztBQXpDVSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEF1dGhlbnRpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9hdXRoZW50aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBVc2VyIH0gZnJvbSAnLi4vZW50aXR5L3VzZXInO1xuXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25zLWxvZ2luJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2xvZ2luLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbG9naW4uY29tcG9uZW50LmNzcyddLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxufSlcbmV4cG9ydCBjbGFzcyBMb2dpbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgdXNlcjpVc2VyID0geyB1c2VybmFtZTonJywgZW1haWw6JycsIHBhc3N3b3JkOicnfTtcbiAgY29uZmlybSA9ICcnO1xuICBzaWdudXAgPScnO1xuICBmZWVkYmFjayA9ICcnO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgYXV0aFNlcnZpY2UgOiBBdXRoZW50aWZpY2F0aW9uU2VydmljZSwgcHJpdmF0ZSByb3V0ZXJFeHQgOiBSb3V0ZXJFeHRlbnNpb25zKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG4gIHJlZ2lzdGVyKCkge1xuICAgIGlmKHRoaXMudXNlci5wYXNzd29yZCA9PT0gdGhpcy5jb25maXJtKSB7XG5cbiAgICAgIHRoaXMuYXV0aFNlcnZpY2UuYWRkVXNlcih0aGlzLnVzZXIpXG4gICAgICAgIC5zdWJzY3JpYmUoXG4gICAgICAgICAgdXNlciA9PiB0aGlzLmxvZ2luVXNlcigpLFxuICAgICAgICAgIGVycm9yID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy5mZWVkYmFjayA9ICdSZWdpc3RlciBlcnJvcic7XG4gICAgICAgICAgfSAsICgpID0+IGNvbnNvbGUubG9nKCdvaycpXG4gICAgICAgICk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuZmVlZGJhY2sgPSAnUGFzc3dvcmQgZGlkIG5vdCBtYXRjaC4nXG4gICAgfVxuXG4gIH1cblxuICBsb2dpblVzZXIoKSB7XG4gICAgdGhpcy5hdXRoU2VydmljZS5sb2dpbih0aGlzLnVzZXIudXNlcm5hbWUsIHRoaXMudXNlci5wYXNzd29yZClcbiAgICAuc3Vic2NyaWJlKFxuICAgICAgKCkgPT4gdGhpcy5yb3V0ZXJFeHQuYmFja1RvUHJldmlvdXNQYWdlKCksXG4gICAgICBlcnJvciA9PiB7XG4gICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgICB0aGlzLmZlZWRiYWNrID0gJ0xvZ2luIGVycm9yLidcbiAgICAgICAgIH1cbiAgICApO1xuICB9XG5cbiAgfVxuXG4iXX0=