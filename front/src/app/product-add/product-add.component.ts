import { Component, OnInit } from '@angular/core';
import { ImageAsset } from "tns-core-modules/image-asset";
import { ProductService } from '../service/product.service';
import { ImageSource } from "tns-core-modules/image-source";
import { takePicture, requestPermissions } from "nativescript-camera";
import * as imagepicker from "nativescript-imagepicker";
import { Product } from '../entity/product';
import { Category } from '../entity/category';
import { ListPicker } from "tns-core-modules/ui/list-picker";
import { CategoryService } from '../service/category.service';


let listTaille = [
  'XS', 'S', 'M', 'L', 'XL', 'XXL'
];

let test = [
  'p', 'p'
]

@Component({
  selector: 'ns-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css'],
  moduleId: module.id,
})

export class ProductAddComponent implements OnInit {

  product: Product = {
    name: '',
    price: 0,
    description: '',
    img: '',
    size: ''
  }


  //affichage taille
  public taille: Array<string> = [];
  public picked: string;


  //affichage cat
  catTab: Category[] = [];
  public cat: Array<string> = [];
  public catPicked: Category;

  
  category: Category = {
    name: ''
  }

  image: ImageAsset;

  constructor(private service: ProductService, private serviceCat: CategoryService) {
    for (let i = 0; i < listTaille.length; i++) {
      this.taille.push(listTaille[i]);
    }

  }


  //souci d'affichage
  ngOnInit() {
    this.serviceCat.findAll().subscribe(data => {
      this.catTab = data;
      for (let i = 0; i < this.catTab.length; i++) {
        this.cat.push(this.catTab[i].name);
      }
    },
      error => console.log(error),
      () => console.log(this.cat)
    );

  }

  //faire method ajout category

  public selectedIndexChanged(args) {
    let picker = <ListPicker>args.object;
    this.picked = this.taille[picker.selectedIndex];
    this.product.size = this.picked;
  }

  public selectedIndexChangedCat(args) {
    let picker = <ListPicker>args.object;
    this.catPicked = this.catTab[picker.selectedIndex];
    this.category.name = this.catPicked.name;
  }



  add() {
    let source = new ImageSource();
    source.fromAsset(this.image).then((source) => {
      const base64image = source.toBase64String("jpg", 70);
      this.product.img = 'data:image/jpeg;base64,' + base64image;
      this.product.category = this.catPicked;
      
      this.service.add(this.product).subscribe(
        resp => console.log(resp),
        error => console.log(error)
      );
    });

  }




  //gestion de l'image

  takePicture() {
    let options = {
      width: 300,
      height: 400,
      keepAspectRatio: true,
      saveToGallery: true
    };
    requestPermissions()
      .then(() => takePicture(options))
      .then(value => {
        this.image = value;

      }).catch(error => console.log(error))
  }

  //selection de l'image

  pickPicture() {
    let picker = imagepicker.create({
      mode: 'single'
    });

    picker.authorize()
      .then(() => picker.present())
      .then((image) => this.image = image[0])
      .catch((error) => console.log(error));
  }


  delete() {
    this.service.delete(this.product).subscribe();
  }

}
