"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var product_service_1 = require("../service/product.service");
var image_source_1 = require("tns-core-modules/image-source");
var nativescript_camera_1 = require("nativescript-camera");
var imagepicker = require("nativescript-imagepicker");
var category_service_1 = require("../service/category.service");
var listTaille = [
    'XS', 'S', 'M', 'L', 'XL', 'XXL'
];
var test = [
    'p', 'p'
];
var ProductAddComponent = /** @class */ (function () {
    function ProductAddComponent(service, serviceCat) {
        this.service = service;
        this.serviceCat = serviceCat;
        this.product = {
            name: '',
            price: 0,
            description: '',
            img: '',
            size: ''
        };
        //affichage taille
        this.taille = [];
        //affichage cat
        this.catTab = [];
        this.cat = [];
        this.category = {
            name: ''
        };
        for (var i = 0; i < listTaille.length; i++) {
            this.taille.push(listTaille[i]);
        }
    }
    //souci d'affichage
    ProductAddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.serviceCat.findAll().subscribe(function (data) {
            _this.catTab = data;
            for (var i = 0; i < _this.catTab.length; i++) {
                _this.cat.push(_this.catTab[i].name);
            }
        }, function (error) { return console.log(error); }, function () { return console.log(_this.cat); });
    };
    //faire method ajout category
    ProductAddComponent.prototype.selectedIndexChanged = function (args) {
        var picker = args.object;
        this.picked = this.taille[picker.selectedIndex];
        this.product.size = this.picked;
    };
    ProductAddComponent.prototype.selectedIndexChangedCat = function (args) {
        var picker = args.object;
        this.catPicked = this.catTab[picker.selectedIndex];
        this.category.name = this.catPicked.name;
    };
    ProductAddComponent.prototype.add = function () {
        var _this = this;
        var source = new image_source_1.ImageSource();
        source.fromAsset(this.image).then(function (source) {
            var base64image = source.toBase64String("jpg", 70);
            _this.product.img = 'data:image/jpeg;base64,' + base64image;
            _this.product.category = _this.catPicked;
            _this.service.add(_this.product).subscribe(function (resp) { return console.log(resp); }, function (error) { return console.log(error); });
        });
    };
    //gestion de l'image
    ProductAddComponent.prototype.takePicture = function () {
        var _this = this;
        var options = {
            width: 300,
            height: 400,
            keepAspectRatio: true,
            saveToGallery: true
        };
        nativescript_camera_1.requestPermissions()
            .then(function () { return nativescript_camera_1.takePicture(options); })
            .then(function (value) {
            _this.image = value;
        }).catch(function (error) { return console.log(error); });
    };
    //selection de l'image
    ProductAddComponent.prototype.pickPicture = function () {
        var _this = this;
        var picker = imagepicker.create({
            mode: 'single'
        });
        picker.authorize()
            .then(function () { return picker.present(); })
            .then(function (image) { return _this.image = image[0]; })
            .catch(function (error) { return console.log(error); });
    };
    ProductAddComponent.prototype.delete = function () {
        this.service.delete(this.product).subscribe();
    };
    ProductAddComponent = __decorate([
        core_1.Component({
            selector: 'ns-product-add',
            templateUrl: './product-add.component.html',
            styleUrls: ['./product-add.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [product_service_1.ProductService, category_service_1.CategoryService])
    ], ProductAddComponent);
    return ProductAddComponent;
}());
exports.ProductAddComponent = ProductAddComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZHVjdC1hZGQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicHJvZHVjdC1hZGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBRWxELDhEQUE0RDtBQUM1RCw4REFBNEQ7QUFDNUQsMkRBQXNFO0FBQ3RFLHNEQUF3RDtBQUl4RCxnRUFBOEQ7QUFHOUQsSUFBSSxVQUFVLEdBQUc7SUFDZixJQUFJLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEtBQUs7Q0FDakMsQ0FBQztBQUVGLElBQUksSUFBSSxHQUFHO0lBQ1QsR0FBRyxFQUFFLEdBQUc7Q0FDVCxDQUFBO0FBU0Q7SUE0QkUsNkJBQW9CLE9BQXVCLEVBQVUsVUFBMkI7UUFBNUQsWUFBTyxHQUFQLE9BQU8sQ0FBZ0I7UUFBVSxlQUFVLEdBQVYsVUFBVSxDQUFpQjtRQTFCaEYsWUFBTyxHQUFZO1lBQ2pCLElBQUksRUFBRSxFQUFFO1lBQ1IsS0FBSyxFQUFFLENBQUM7WUFDUixXQUFXLEVBQUUsRUFBRTtZQUNmLEdBQUcsRUFBRSxFQUFFO1lBQ1AsSUFBSSxFQUFFLEVBQUU7U0FDVCxDQUFBO1FBR0Qsa0JBQWtCO1FBQ1gsV0FBTSxHQUFrQixFQUFFLENBQUM7UUFJbEMsZUFBZTtRQUNmLFdBQU0sR0FBZSxFQUFFLENBQUM7UUFDakIsUUFBRyxHQUFrQixFQUFFLENBQUM7UUFJL0IsYUFBUSxHQUFhO1lBQ25CLElBQUksRUFBRSxFQUFFO1NBQ1QsQ0FBQTtRQUtDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQzNDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xDLENBQUM7SUFFSCxDQUFDO0lBR0QsbUJBQW1CO0lBQ25CLHNDQUFRLEdBQVI7UUFBQSxpQkFXQztRQVZDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsSUFBSTtZQUN0QyxLQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNuQixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQzVDLEtBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDckMsQ0FBQztRQUNILENBQUMsRUFDQyxVQUFBLEtBQUssSUFBSSxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQWxCLENBQWtCLEVBQzNCLGNBQU0sT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsQ0FDNUIsQ0FBQztJQUVKLENBQUM7SUFFRCw2QkFBNkI7SUFFdEIsa0RBQW9CLEdBQTNCLFVBQTRCLElBQUk7UUFDOUIsSUFBSSxNQUFNLEdBQWUsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDbEMsQ0FBQztJQUVNLHFEQUF1QixHQUE5QixVQUErQixJQUFJO1FBQ2pDLElBQUksTUFBTSxHQUFlLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDckMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztJQUMzQyxDQUFDO0lBSUQsaUNBQUcsR0FBSDtRQUFBLGlCQWFDO1FBWkMsSUFBSSxNQUFNLEdBQUcsSUFBSSwwQkFBVyxFQUFFLENBQUM7UUFDL0IsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsTUFBTTtZQUN2QyxJQUFNLFdBQVcsR0FBRyxNQUFNLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztZQUNyRCxLQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsR0FBRyx5QkFBeUIsR0FBRyxXQUFXLENBQUM7WUFDM0QsS0FBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQztZQUV2QyxLQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsU0FBUyxDQUN0QyxVQUFBLElBQUksSUFBSSxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQWpCLENBQWlCLEVBQ3pCLFVBQUEsS0FBSyxJQUFJLE9BQUEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBbEIsQ0FBa0IsQ0FDNUIsQ0FBQztRQUNKLENBQUMsQ0FBQyxDQUFDO0lBRUwsQ0FBQztJQUtELG9CQUFvQjtJQUVwQix5Q0FBVyxHQUFYO1FBQUEsaUJBYUM7UUFaQyxJQUFJLE9BQU8sR0FBRztZQUNaLEtBQUssRUFBRSxHQUFHO1lBQ1YsTUFBTSxFQUFFLEdBQUc7WUFDWCxlQUFlLEVBQUUsSUFBSTtZQUNyQixhQUFhLEVBQUUsSUFBSTtTQUNwQixDQUFDO1FBQ0Ysd0NBQWtCLEVBQUU7YUFDakIsSUFBSSxDQUFDLGNBQU0sT0FBQSxpQ0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFwQixDQUFvQixDQUFDO2FBQ2hDLElBQUksQ0FBQyxVQUFBLEtBQUs7WUFDVCxLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUVyQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFsQixDQUFrQixDQUFDLENBQUE7SUFDekMsQ0FBQztJQUVELHNCQUFzQjtJQUV0Qix5Q0FBVyxHQUFYO1FBQUEsaUJBU0M7UUFSQyxJQUFJLE1BQU0sR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDO1lBQzlCLElBQUksRUFBRSxRQUFRO1NBQ2YsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLFNBQVMsRUFBRTthQUNmLElBQUksQ0FBQyxjQUFNLE9BQUEsTUFBTSxDQUFDLE9BQU8sRUFBRSxFQUFoQixDQUFnQixDQUFDO2FBQzVCLElBQUksQ0FBQyxVQUFDLEtBQUssSUFBSyxPQUFBLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFyQixDQUFxQixDQUFDO2FBQ3RDLEtBQUssQ0FBQyxVQUFDLEtBQUssSUFBSyxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQWxCLENBQWtCLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBR0Qsb0NBQU0sR0FBTjtRQUNFLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNoRCxDQUFDO0lBckhVLG1CQUFtQjtRQVAvQixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGdCQUFnQjtZQUMxQixXQUFXLEVBQUUsOEJBQThCO1lBQzNDLFNBQVMsRUFBRSxDQUFDLDZCQUE2QixDQUFDO1lBQzFDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUNwQixDQUFDO3lDQThCNkIsZ0NBQWMsRUFBc0Isa0NBQWU7T0E1QnJFLG1CQUFtQixDQXVIL0I7SUFBRCwwQkFBQztDQUFBLEFBdkhELElBdUhDO0FBdkhZLGtEQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBJbWFnZUFzc2V0IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvaW1hZ2UtYXNzZXRcIjtcbmltcG9ydCB7IFByb2R1Y3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9wcm9kdWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgSW1hZ2VTb3VyY2UgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9pbWFnZS1zb3VyY2VcIjtcbmltcG9ydCB7IHRha2VQaWN0dXJlLCByZXF1ZXN0UGVybWlzc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWNhbWVyYVwiO1xuaW1wb3J0ICogYXMgaW1hZ2VwaWNrZXIgZnJvbSBcIm5hdGl2ZXNjcmlwdC1pbWFnZXBpY2tlclwiO1xuaW1wb3J0IHsgUHJvZHVjdCB9IGZyb20gJy4uL2VudGl0eS9wcm9kdWN0JztcbmltcG9ydCB7IENhdGVnb3J5IH0gZnJvbSAnLi4vZW50aXR5L2NhdGVnb3J5JztcbmltcG9ydCB7IExpc3RQaWNrZXIgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9saXN0LXBpY2tlclwiO1xuaW1wb3J0IHsgQ2F0ZWdvcnlTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9jYXRlZ29yeS5zZXJ2aWNlJztcblxuXG5sZXQgbGlzdFRhaWxsZSA9IFtcbiAgJ1hTJywgJ1MnLCAnTScsICdMJywgJ1hMJywgJ1hYTCdcbl07XG5cbmxldCB0ZXN0ID0gW1xuICAncCcsICdwJ1xuXVxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICducy1wcm9kdWN0LWFkZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9wcm9kdWN0LWFkZC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3Byb2R1Y3QtYWRkLmNvbXBvbmVudC5jc3MnXSxcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbn0pXG5cbmV4cG9ydCBjbGFzcyBQcm9kdWN0QWRkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBwcm9kdWN0OiBQcm9kdWN0ID0ge1xuICAgIG5hbWU6ICcnLFxuICAgIHByaWNlOiAwLFxuICAgIGRlc2NyaXB0aW9uOiAnJyxcbiAgICBpbWc6ICcnLFxuICAgIHNpemU6ICcnXG4gIH1cblxuXG4gIC8vYWZmaWNoYWdlIHRhaWxsZVxuICBwdWJsaWMgdGFpbGxlOiBBcnJheTxzdHJpbmc+ID0gW107XG4gIHB1YmxpYyBwaWNrZWQ6IHN0cmluZztcblxuXG4gIC8vYWZmaWNoYWdlIGNhdFxuICBjYXRUYWI6IENhdGVnb3J5W10gPSBbXTtcbiAgcHVibGljIGNhdDogQXJyYXk8c3RyaW5nPiA9IFtdO1xuICBwdWJsaWMgY2F0UGlja2VkOiBDYXRlZ29yeTtcblxuICBcbiAgY2F0ZWdvcnk6IENhdGVnb3J5ID0ge1xuICAgIG5hbWU6ICcnXG4gIH1cblxuICBpbWFnZTogSW1hZ2VBc3NldDtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNlcnZpY2U6IFByb2R1Y3RTZXJ2aWNlLCBwcml2YXRlIHNlcnZpY2VDYXQ6IENhdGVnb3J5U2VydmljZSkge1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbGlzdFRhaWxsZS5sZW5ndGg7IGkrKykge1xuICAgICAgdGhpcy50YWlsbGUucHVzaChsaXN0VGFpbGxlW2ldKTtcbiAgICB9XG5cbiAgfVxuXG5cbiAgLy9zb3VjaSBkJ2FmZmljaGFnZVxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLnNlcnZpY2VDYXQuZmluZEFsbCgpLnN1YnNjcmliZShkYXRhID0+IHtcbiAgICAgIHRoaXMuY2F0VGFiID0gZGF0YTtcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5jYXRUYWIubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdGhpcy5jYXQucHVzaCh0aGlzLmNhdFRhYltpXS5uYW1lKTtcbiAgICAgIH1cbiAgICB9LFxuICAgICAgZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpLFxuICAgICAgKCkgPT4gY29uc29sZS5sb2codGhpcy5jYXQpXG4gICAgKTtcblxuICB9XG5cbiAgLy9mYWlyZSBtZXRob2QgYWpvdXQgY2F0ZWdvcnlcblxuICBwdWJsaWMgc2VsZWN0ZWRJbmRleENoYW5nZWQoYXJncykge1xuICAgIGxldCBwaWNrZXIgPSA8TGlzdFBpY2tlcj5hcmdzLm9iamVjdDtcbiAgICB0aGlzLnBpY2tlZCA9IHRoaXMudGFpbGxlW3BpY2tlci5zZWxlY3RlZEluZGV4XTtcbiAgICB0aGlzLnByb2R1Y3Quc2l6ZSA9IHRoaXMucGlja2VkO1xuICB9XG5cbiAgcHVibGljIHNlbGVjdGVkSW5kZXhDaGFuZ2VkQ2F0KGFyZ3MpIHtcbiAgICBsZXQgcGlja2VyID0gPExpc3RQaWNrZXI+YXJncy5vYmplY3Q7XG4gICAgdGhpcy5jYXRQaWNrZWQgPSB0aGlzLmNhdFRhYltwaWNrZXIuc2VsZWN0ZWRJbmRleF07XG4gICAgdGhpcy5jYXRlZ29yeS5uYW1lID0gdGhpcy5jYXRQaWNrZWQubmFtZTtcbiAgfVxuXG5cblxuICBhZGQoKSB7XG4gICAgbGV0IHNvdXJjZSA9IG5ldyBJbWFnZVNvdXJjZSgpO1xuICAgIHNvdXJjZS5mcm9tQXNzZXQodGhpcy5pbWFnZSkudGhlbigoc291cmNlKSA9PiB7XG4gICAgICBjb25zdCBiYXNlNjRpbWFnZSA9IHNvdXJjZS50b0Jhc2U2NFN0cmluZyhcImpwZ1wiLCA3MCk7XG4gICAgICB0aGlzLnByb2R1Y3QuaW1nID0gJ2RhdGE6aW1hZ2UvanBlZztiYXNlNjQsJyArIGJhc2U2NGltYWdlO1xuICAgICAgdGhpcy5wcm9kdWN0LmNhdGVnb3J5ID0gdGhpcy5jYXRQaWNrZWQ7XG4gICAgICBcbiAgICAgIHRoaXMuc2VydmljZS5hZGQodGhpcy5wcm9kdWN0KS5zdWJzY3JpYmUoXG4gICAgICAgIHJlc3AgPT4gY29uc29sZS5sb2cocmVzcCksXG4gICAgICAgIGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKVxuICAgICAgKTtcbiAgICB9KTtcblxuICB9XG5cblxuXG5cbiAgLy9nZXN0aW9uIGRlIGwnaW1hZ2VcblxuICB0YWtlUGljdHVyZSgpIHtcbiAgICBsZXQgb3B0aW9ucyA9IHtcbiAgICAgIHdpZHRoOiAzMDAsXG4gICAgICBoZWlnaHQ6IDQwMCxcbiAgICAgIGtlZXBBc3BlY3RSYXRpbzogdHJ1ZSxcbiAgICAgIHNhdmVUb0dhbGxlcnk6IHRydWVcbiAgICB9O1xuICAgIHJlcXVlc3RQZXJtaXNzaW9ucygpXG4gICAgICAudGhlbigoKSA9PiB0YWtlUGljdHVyZShvcHRpb25zKSlcbiAgICAgIC50aGVuKHZhbHVlID0+IHtcbiAgICAgICAgdGhpcy5pbWFnZSA9IHZhbHVlO1xuXG4gICAgICB9KS5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpXG4gIH1cblxuICAvL3NlbGVjdGlvbiBkZSBsJ2ltYWdlXG5cbiAgcGlja1BpY3R1cmUoKSB7XG4gICAgbGV0IHBpY2tlciA9IGltYWdlcGlja2VyLmNyZWF0ZSh7XG4gICAgICBtb2RlOiAnc2luZ2xlJ1xuICAgIH0pO1xuXG4gICAgcGlja2VyLmF1dGhvcml6ZSgpXG4gICAgICAudGhlbigoKSA9PiBwaWNrZXIucHJlc2VudCgpKVxuICAgICAgLnRoZW4oKGltYWdlKSA9PiB0aGlzLmltYWdlID0gaW1hZ2VbMF0pXG4gICAgICAuY2F0Y2goKGVycm9yKSA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuICB9XG5cblxuICBkZWxldGUoKSB7XG4gICAgdGhpcy5zZXJ2aWNlLmRlbGV0ZSh0aGlzLnByb2R1Y3QpLnN1YnNjcmliZSgpO1xuICB9XG5cbn1cbiJdfQ==