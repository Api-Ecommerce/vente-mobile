import { Injectable } from '@angular/core';
import { environment } from '~/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../entity/product';
import { Category } from '../entity/category';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  apiUrl = environment.serverUrl + '/api/product/';

  constructor(private http: HttpClient) { }

  findAll(): Observable<Product[]> {
    return this.http.get<Product[]>(this.apiUrl);
  }

  //trouve un product
  find(id: number): Observable<Product> {
    return this.http.get<Product>(this.apiUrl + 'single/' + id);
  }

  //trouve product par rapport a user
  findByUser(): Observable<Product[]> {
    return this.http.get<Product[]>(this.apiUrl + 'productUserSale');
  }

  add(product: Product): Observable<Product> {
    let data: any = Object.assign({}, product);
    if (product.category) {
      data.category = product.category.id;
    }
    return this.http.post<Product>(this.apiUrl, data);
  }

  delete(product: Product) {
    return this.http.delete(this.apiUrl + product.id);
  }

  update(product: Product): Observable<Product> {
    return this.http.put<Product>(this.apiUrl + product.id, product);

  }

  findByCat(id:number): Observable<Product[]>{
    return this.http.get<Product[]>(this.apiUrl + 'catProduct/' + id);
  }



}
