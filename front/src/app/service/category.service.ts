import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Category } from '../entity/category';

import {environment} from '../../environments/environment';





@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  apiUrl = environment.serverUrl + '/api/category';

  constructor(private Http:HttpClient) { }

  findAll():Observable<Category[]>{
    return this.Http.get<Category[]>(this.apiUrl);
  }

  find(id:number):Observable<Category>{
    return this.Http.get<Category>(this.apiUrl + '/catChoise/' + id);
  }

  add(Category:Category):Observable<Category> {
    return this.Http.post<Category>(this.apiUrl , Category)
  }

  delete(Category:Category){
    return this.Http.delete(this.apiUrl + Category.id);
  }

  pushInCat(){

  }
}
