"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var appSetting = require("application-settings");
var environment_1 = require("../../environments/environment");
var http_1 = require("@angular/common/http");
var operators_1 = require("rxjs/operators");
var AuthentificationService = /** @class */ (function () {
    function AuthentificationService(http) {
        this.http = http;
        this.apiUrl = environment_1.environment.serverUrl + '/api/user';
    }
    AuthentificationService.prototype.addUser = function (user) {
        return this.http.post(this.apiUrl, user);
    };
    // ok
    AuthentificationService.prototype.login = function (username, password) {
        return this.http.post(environment_1.environment.serverUrl + '/api/login_check', {
            username: username,
            password: password
        }).pipe(operators_1.tap(function (data) {
            if (data.token) {
                appSetting.setString('token', data.token);
            }
        }));
    };
    AuthentificationService.prototype.logout = function () {
        appSetting.remove('token');
    };
    AuthentificationService.prototype.isLogged = function () {
        return appSetting.hasKey('token');
    };
    AuthentificationService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], AuthentificationService);
    return AuthentificationService;
}());
exports.AuthentificationService = AuthentificationService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGlmaWNhdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXV0aGVudGlmaWNhdGlvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLGlEQUFtRDtBQUNuRCw4REFBNkQ7QUFDN0QsNkNBQWtEO0FBR2xELDRDQUFxQztBQU1yQztJQUlFLGlDQUFvQixJQUFlO1FBQWYsU0FBSSxHQUFKLElBQUksQ0FBVztRQUYzQixXQUFNLEdBQUcseUJBQVcsQ0FBQyxTQUFTLEdBQUMsV0FBVyxDQUFDO0lBRVosQ0FBQztJQUN4Qyx5Q0FBTyxHQUFQLFVBQVEsSUFBUztRQUNmLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBTyxJQUFJLENBQUMsTUFBTSxFQUFHLElBQUksQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFRCxLQUFLO0lBRUwsdUNBQUssR0FBTCxVQUFNLFFBQWUsRUFBRSxRQUFlO1FBQ3BDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBTSx5QkFBVyxDQUFDLFNBQVMsR0FBRSxrQkFBa0IsRUFBRTtZQUNwRSxRQUFRLEVBQUUsUUFBUTtZQUNsQixRQUFRLEVBQUUsUUFBUTtTQUNuQixDQUFDLENBQUMsSUFBSSxDQUNMLGVBQUcsQ0FBQyxVQUFBLElBQUk7WUFDTixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDZCxVQUFVLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDNUMsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUNILENBQUM7SUFDSixDQUFDO0lBRUQsd0NBQU0sR0FBTjtRQUNFLFVBQVUsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7SUFFN0IsQ0FBQztJQUVBLDBDQUFRLEdBQVI7UUFDQyxNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBL0JVLHVCQUF1QjtRQUpuQyxpQkFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQzt5Q0FNeUIsaUJBQVU7T0FKeEIsdUJBQXVCLENBa0NuQztJQUFELDhCQUFDO0NBQUEsQUFsQ0QsSUFrQ0M7QUFsQ1ksMERBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0ICogYXMgYXBwU2V0dGluZyBmcm9tICdhcHBsaWNhdGlvbi1zZXR0aW5ncyc7XG5pbXBvcnQgeyBlbnZpcm9ubWVudCB9IGZyb20gJy4uLy4uL2Vudmlyb25tZW50cy9lbnZpcm9ubWVudCc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgVXNlciB9IGZyb20gJy4uL2VudGl0eS91c2VyJztcbmltcG9ydCB7IE9ic2VydmFibGUsIEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgdGFwIH0gZnJvbSBcInJ4anMvb3BlcmF0b3JzXCI7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuXG5leHBvcnQgY2xhc3MgQXV0aGVudGlmaWNhdGlvblNlcnZpY2Uge1xuICBcbiAgcHJpdmF0ZSBhcGlVcmwgPSBlbnZpcm9ubWVudC5zZXJ2ZXJVcmwrJy9hcGkvdXNlcic7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOkh0dHBDbGllbnQpIHsgfVxuICBhZGRVc2VyKHVzZXI6VXNlcik6IE9ic2VydmFibGU8VXNlcj4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdDxVc2VyPih0aGlzLmFwaVVybCAsIHVzZXIpO1xuICB9XG5cbiAgLy8gb2tcblxuICBsb2dpbih1c2VybmFtZTpzdHJpbmcsIHBhc3N3b3JkOnN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0PGFueT4oZW52aXJvbm1lbnQuc2VydmVyVXJsKyAnL2FwaS9sb2dpbl9jaGVjaycsIHtcbiAgICAgIHVzZXJuYW1lOiB1c2VybmFtZSxcbiAgICAgIHBhc3N3b3JkOiBwYXNzd29yZFxuICAgIH0pLnBpcGUoXG4gICAgICB0YXAoZGF0YSA9PiB7XG4gICAgICAgIGlmKGRhdGEudG9rZW4pIHtcbiAgICAgICAgICBhcHBTZXR0aW5nLnNldFN0cmluZygndG9rZW4nLCBkYXRhLnRva2VuKTtcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICApO1xuICB9XG4gIFxuICBsb2dvdXQoKSB7XG4gICAgYXBwU2V0dGluZy5yZW1vdmUoJ3Rva2VuJyk7XG5cbiAgfVxuXG4gICBpc0xvZ2dlZCgpIHtcbiAgICByZXR1cm4gYXBwU2V0dGluZy5oYXNLZXkoJ3Rva2VuJyk7XG4gIH1cblxuIFxufVxuIl19