"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var environment_1 = require("~/environments/environment");
var http_1 = require("@angular/common/http");
var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
        this.apiUrl = environment_1.environment.serverUrl + '/api/user/';
    }
    UserService.prototype.findByUser = function () {
        return this.http.get(this.apiUrl + "user");
    };
    UserService.prototype.findUserSale = function () {
        return this.http.get(this.apiUrl + "getBySale");
    };
    UserService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidXNlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLDBEQUF5RDtBQUN6RCw2Q0FBa0Q7QUFRbEQ7SUFJRSxxQkFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUZwQyxXQUFNLEdBQUcseUJBQVcsQ0FBQyxTQUFTLEdBQUcsWUFBWSxDQUFDO0lBRU4sQ0FBQztJQUd6QyxnQ0FBVSxHQUFWO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFPLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUE7SUFDbEQsQ0FBQztJQUVELGtDQUFZLEdBQVo7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQVksSUFBSSxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBYlUsV0FBVztRQUh2QixpQkFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQzt5Q0FLMEIsaUJBQVU7T0FKekIsV0FBVyxDQWV2QjtJQUFELGtCQUFDO0NBQUEsQUFmRCxJQWVDO0FBZlksa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBlbnZpcm9ubWVudCB9IGZyb20gJ34vZW52aXJvbm1lbnRzL2Vudmlyb25tZW50JztcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBVc2VyIH0gZnJvbSAnLi4vZW50aXR5L3VzZXInO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgUHJvZHVjdCB9IGZyb20gJy4uL2VudGl0eS9wcm9kdWN0JztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgVXNlclNlcnZpY2Uge1xuXG4gIGFwaVVybCA9IGVudmlyb25tZW50LnNlcnZlclVybCArICcvYXBpL3VzZXIvJztcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHsgfVxuXG5cbiAgZmluZEJ5VXNlcigpOiBPYnNlcnZhYmxlPFVzZXI+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldDxVc2VyPih0aGlzLmFwaVVybCArIFwidXNlclwiKVxuICB9XG5cbiAgZmluZFVzZXJTYWxlKCk6IE9ic2VydmFibGU8UHJvZHVjdFtdPiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQ8UHJvZHVjdFtdPih0aGlzLmFwaVVybCArIFwiZ2V0QnlTYWxlXCIpO1xuICB9XG5cbn1cbiJdfQ==