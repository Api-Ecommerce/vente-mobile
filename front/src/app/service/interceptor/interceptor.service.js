"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var appSetting = require("application-settings");
var authentification_service_1 = require("../authentification.service");
var InterceptorService = /** @class */ (function () {
    function InterceptorService(auth) {
        this.auth = auth;
    }
    InterceptorService.prototype.intercept = function (req, next) {
        var _this = this;
        var token = appSetting.getString('token');
        if (token) {
            //si on a le token, on clone la requête interceptée.
            var modifiedRequest = req.clone({
                //Dans la requête clonée, on rajoute les headers qu'on veut
                setHeaders: {
                    //en l'occurrence, le Authorization Bearer avec token
                    Authorization: 'Bearer ' + token
                }
            });
            //puis on donne au httpClient la requête modifiée
            return next.handle(modifiedRequest)
                .pipe(
            //Et on lui dit de catcher les erreurs de cette requête
            operators_1.catchError(function (err, caught) {
                //Si l'erreur a un status 401, ça veut dire que notre token
                //est invalide
                if (err.status === 401) {
                    //On dit donc au service authentication de nous logout
                    _this.auth.logout();
                }
                //puis on fait suivre l'erreur
                return rxjs_1.throwError(err);
            }));
        }
        //si ya pas de token, on repasse la requête sans y toucher
        return next.handle(req);
    };
    InterceptorService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [authentification_service_1.AuthentificationService])
    ], InterceptorService);
    return InterceptorService;
}());
exports.InterceptorService = InterceptorService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJjZXB0b3Iuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImludGVyY2VwdG9yLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFFM0MsNkJBQThDO0FBQzlDLDRDQUE0QztBQUU1QyxpREFBbUQ7QUFDbkQsd0VBQXNFO0FBS3RFO0lBRUUsNEJBQW9CLElBQTRCO1FBQTVCLFNBQUksR0FBSixJQUFJLENBQXdCO0lBQUksQ0FBQztJQUVyRCxzQ0FBUyxHQUFULFVBQVUsR0FBcUIsRUFDckIsSUFBaUI7UUFEM0IsaUJBK0JDO1FBN0JDLElBQUksS0FBSyxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDMUMsRUFBRSxDQUFBLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNULG9EQUFvRDtZQUNwRCxJQUFJLGVBQWUsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO2dCQUM5QiwyREFBMkQ7Z0JBQzNELFVBQVUsRUFBRTtvQkFDVixxREFBcUQ7b0JBQ3JELGFBQWEsRUFBRSxTQUFTLEdBQUMsS0FBSztpQkFDL0I7YUFDRixDQUFDLENBQUM7WUFDSCxpREFBaUQ7WUFDakQsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDO2lCQUVsQyxJQUFJO1lBQ0gsdURBQXVEO1lBQ3ZELHNCQUFVLENBQUMsVUFBQyxHQUFHLEVBQUUsTUFBTTtnQkFDckIsMkRBQTJEO2dCQUMzRCxjQUFjO2dCQUNkLEVBQUUsQ0FBQSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDdEIsc0RBQXNEO29CQUN0RCxLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUNyQixDQUFDO2dCQUNELDhCQUE4QjtnQkFDOUIsTUFBTSxDQUFDLGlCQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDekIsQ0FBQyxDQUFDLENBQ0gsQ0FBQztRQUNKLENBQUM7UUFDRCwwREFBMEQ7UUFDMUQsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQW5DVSxrQkFBa0I7UUFIOUIsaUJBQVUsQ0FBQztZQUNWLFVBQVUsRUFBRSxNQUFNO1NBQ25CLENBQUM7eUNBR3lCLGtEQUF1QjtPQUZyQyxrQkFBa0IsQ0FxQzlCO0lBQUQseUJBQUM7Q0FBQSxBQXJDRCxJQXFDQztBQXJDWSxnREFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwSW50ZXJjZXB0b3IsIEh0dHBSZXF1ZXN0LCBIdHRwSGFuZGxlciB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUsIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbmltcG9ydCAqIGFzIGFwcFNldHRpbmcgZnJvbSAnYXBwbGljYXRpb24tc2V0dGluZ3MnO1xuaW1wb3J0IHsgQXV0aGVudGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi9hdXRoZW50aWZpY2F0aW9uLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBJbnRlcmNlcHRvclNlcnZpY2UgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3J7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBhdXRoOkF1dGhlbnRpZmljYXRpb25TZXJ2aWNlKSB7IH1cblxuICBpbnRlcmNlcHQocmVxOiBIdHRwUmVxdWVzdDxhbnk+LCBcbiAgICAgICAgICAgIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBsZXQgdG9rZW4gPSBhcHBTZXR0aW5nLmdldFN0cmluZygndG9rZW4nKTtcbiAgICBpZih0b2tlbikge1xuICAgICAgLy9zaSBvbiBhIGxlIHRva2VuLCBvbiBjbG9uZSBsYSByZXF1w6p0ZSBpbnRlcmNlcHTDqWUuXG4gICAgICBsZXQgbW9kaWZpZWRSZXF1ZXN0ID0gcmVxLmNsb25lKHtcbiAgICAgICAgLy9EYW5zIGxhIHJlcXXDqnRlIGNsb27DqWUsIG9uIHJham91dGUgbGVzIGhlYWRlcnMgcXUnb24gdmV1dFxuICAgICAgICBzZXRIZWFkZXJzOiB7XG4gICAgICAgICAgLy9lbiBsJ29jY3VycmVuY2UsIGxlIEF1dGhvcml6YXRpb24gQmVhcmVyIGF2ZWMgdG9rZW5cbiAgICAgICAgICBBdXRob3JpemF0aW9uOiAnQmVhcmVyICcrdG9rZW5cbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgICAvL3B1aXMgb24gZG9ubmUgYXUgaHR0cENsaWVudCBsYSByZXF1w6p0ZSBtb2RpZmnDqWVcbiAgICAgIHJldHVybiBuZXh0LmhhbmRsZShtb2RpZmllZFJlcXVlc3QpXG4gICAgICAvL09uIHJham91dGUgdW4gcGlwZSBzdXIgbCdPYnNlcnZhYmxlIHF1aSBzdXJ2ZWlsbGUgbGEgcmVxdcOqdGVcbiAgICAgIC5waXBlKFxuICAgICAgICAvL0V0IG9uIGx1aSBkaXQgZGUgY2F0Y2hlciBsZXMgZXJyZXVycyBkZSBjZXR0ZSByZXF1w6p0ZVxuICAgICAgICBjYXRjaEVycm9yKChlcnIsIGNhdWdodCkgPT4ge1xuICAgICAgICAgIC8vU2kgbCdlcnJldXIgYSB1biBzdGF0dXMgNDAxLCDDp2EgdmV1dCBkaXJlIHF1ZSBub3RyZSB0b2tlblxuICAgICAgICAgIC8vZXN0IGludmFsaWRlXG4gICAgICAgICAgaWYoZXJyLnN0YXR1cyA9PT0gNDAxKSB7XG4gICAgICAgICAgICAvL09uIGRpdCBkb25jIGF1IHNlcnZpY2UgYXV0aGVudGljYXRpb24gZGUgbm91cyBsb2dvdXRcbiAgICAgICAgICAgIHRoaXMuYXV0aC5sb2dvdXQoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgLy9wdWlzIG9uIGZhaXQgc3VpdnJlIGwnZXJyZXVyXG4gICAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyKTtcbiAgICAgICAgfSlcbiAgICAgICk7XG4gICAgfVxuICAgIC8vc2kgeWEgcGFzIGRlIHRva2VuLCBvbiByZXBhc3NlIGxhIHJlcXXDqnRlIHNhbnMgeSB0b3VjaGVyXG4gICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcSk7XG4gIH1cbiAgXG59XG4iXX0=