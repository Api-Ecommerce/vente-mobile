"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var environment_1 = require("../../environments/environment");
var CategoryService = /** @class */ (function () {
    function CategoryService(Http) {
        this.Http = Http;
        this.apiUrl = environment_1.environment.serverUrl + '/api/category';
    }
    CategoryService.prototype.findAll = function () {
        return this.Http.get(this.apiUrl);
    };
    CategoryService.prototype.find = function (id) {
        return this.Http.get(this.apiUrl + '/catChoise/' + id);
    };
    CategoryService.prototype.add = function (Category) {
        return this.Http.post(this.apiUrl, Category);
    };
    CategoryService.prototype.delete = function (Category) {
        return this.Http.delete(this.apiUrl + Category.id);
    };
    CategoryService.prototype.pushInCat = function () {
    };
    CategoryService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], CategoryService);
    return CategoryService;
}());
exports.CategoryService = CategoryService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2F0ZWdvcnkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNhdGVnb3J5LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsNkNBQWtEO0FBSWxELDhEQUEyRDtBQVMzRDtJQUlFLHlCQUFvQixJQUFlO1FBQWYsU0FBSSxHQUFKLElBQUksQ0FBVztRQUZuQyxXQUFNLEdBQUcseUJBQVcsQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO0lBRVYsQ0FBQztJQUV4QyxpQ0FBTyxHQUFQO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFhLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQsOEJBQUksR0FBSixVQUFLLEVBQVM7UUFDWixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQVcsSUFBSSxDQUFDLE1BQU0sR0FBRyxhQUFhLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDbkUsQ0FBQztJQUVELDZCQUFHLEdBQUgsVUFBSSxRQUFpQjtRQUNuQixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQVcsSUFBSSxDQUFDLE1BQU0sRUFBRyxRQUFRLENBQUMsQ0FBQTtJQUN6RCxDQUFDO0lBRUQsZ0NBQU0sR0FBTixVQUFPLFFBQWlCO1FBQ3RCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQsbUNBQVMsR0FBVDtJQUVBLENBQUM7SUF4QlUsZUFBZTtRQUgzQixpQkFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQzt5Q0FLeUIsaUJBQVU7T0FKeEIsZUFBZSxDQXlCM0I7SUFBRCxzQkFBQztDQUFBLEFBekJELElBeUJDO0FBekJZLDBDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IENhdGVnb3J5IH0gZnJvbSAnLi4vZW50aXR5L2NhdGVnb3J5JztcblxuaW1wb3J0IHtlbnZpcm9ubWVudH0gZnJvbSAnLi4vLi4vZW52aXJvbm1lbnRzL2Vudmlyb25tZW50JztcblxuXG5cblxuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBDYXRlZ29yeVNlcnZpY2Uge1xuXG4gIGFwaVVybCA9IGVudmlyb25tZW50LnNlcnZlclVybCArICcvYXBpL2NhdGVnb3J5JztcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIEh0dHA6SHR0cENsaWVudCkgeyB9XG5cbiAgZmluZEFsbCgpOk9ic2VydmFibGU8Q2F0ZWdvcnlbXT57XG4gICAgcmV0dXJuIHRoaXMuSHR0cC5nZXQ8Q2F0ZWdvcnlbXT4odGhpcy5hcGlVcmwpO1xuICB9XG5cbiAgZmluZChpZDpudW1iZXIpOk9ic2VydmFibGU8Q2F0ZWdvcnk+e1xuICAgIHJldHVybiB0aGlzLkh0dHAuZ2V0PENhdGVnb3J5Pih0aGlzLmFwaVVybCArICcvY2F0Q2hvaXNlLycgKyBpZCk7XG4gIH1cblxuICBhZGQoQ2F0ZWdvcnk6Q2F0ZWdvcnkpOk9ic2VydmFibGU8Q2F0ZWdvcnk+IHtcbiAgICByZXR1cm4gdGhpcy5IdHRwLnBvc3Q8Q2F0ZWdvcnk+KHRoaXMuYXBpVXJsICwgQ2F0ZWdvcnkpXG4gIH1cblxuICBkZWxldGUoQ2F0ZWdvcnk6Q2F0ZWdvcnkpe1xuICAgIHJldHVybiB0aGlzLkh0dHAuZGVsZXRlKHRoaXMuYXBpVXJsICsgQ2F0ZWdvcnkuaWQpO1xuICB9XG5cbiAgcHVzaEluQ2F0KCl7XG5cbiAgfVxufVxuIl19