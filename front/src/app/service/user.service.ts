import { Injectable } from '@angular/core';
import { environment } from '~/environments/environment';
import { HttpClient } from '@angular/common/http';
import { User } from '../entity/user';
import { Observable } from 'rxjs';
import { Product } from '../entity/product';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiUrl = environment.serverUrl + '/api/user/';

  constructor(private http: HttpClient) { }


  findByUser(): Observable<User> {
    return this.http.get<User>(this.apiUrl + "user")
  }

  findUserSale(): Observable<Product[]> {
    return this.http.get<Product[]>(this.apiUrl + "getBySale");
  }

}
