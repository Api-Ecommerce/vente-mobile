"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var environment_1 = require("~/environments/environment");
var http_1 = require("@angular/common/http");
var ProductService = /** @class */ (function () {
    function ProductService(http) {
        this.http = http;
        this.apiUrl = environment_1.environment.serverUrl + '/api/product/';
    }
    ProductService.prototype.findAll = function () {
        return this.http.get(this.apiUrl);
    };
    //trouve un product
    ProductService.prototype.find = function (id) {
        return this.http.get(this.apiUrl + 'single/' + id);
    };
    //trouve product par rapport a user
    ProductService.prototype.findByUser = function () {
        return this.http.get(this.apiUrl + 'productUserSale');
    };
    ProductService.prototype.add = function (product) {
        var data = Object.assign({}, product);
        if (product.category) {
            data.category = product.category.id;
        }
        return this.http.post(this.apiUrl, data);
    };
    ProductService.prototype.delete = function (product) {
        return this.http.delete(this.apiUrl + product.id);
    };
    ProductService.prototype.update = function (product) {
        return this.http.put(this.apiUrl + product.id, product);
    };
    ProductService.prototype.findByCat = function (id) {
        return this.http.get(this.apiUrl + 'catProduct/' + id);
    };
    ProductService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], ProductService);
    return ProductService;
}());
exports.ProductService = ProductService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZHVjdC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicHJvZHVjdC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLDBEQUF5RDtBQUN6RCw2Q0FBa0Q7QUFRbEQ7SUFJRSx3QkFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUZwQyxXQUFNLEdBQUcseUJBQVcsQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO0lBRVQsQ0FBQztJQUV6QyxnQ0FBTyxHQUFQO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFZLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBRUQsbUJBQW1CO0lBQ25CLDZCQUFJLEdBQUosVUFBSyxFQUFVO1FBQ2IsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFVLElBQUksQ0FBQyxNQUFNLEdBQUcsU0FBUyxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCxtQ0FBbUM7SUFDbkMsbUNBQVUsR0FBVjtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBWSxJQUFJLENBQUMsTUFBTSxHQUFHLGlCQUFpQixDQUFDLENBQUM7SUFDbkUsQ0FBQztJQUVELDRCQUFHLEdBQUgsVUFBSSxPQUFnQjtRQUNsQixJQUFJLElBQUksR0FBUSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUMzQyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUNyQixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDO1FBQ3RDLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQVUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsK0JBQU0sR0FBTixVQUFPLE9BQWdCO1FBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsK0JBQU0sR0FBTixVQUFPLE9BQWdCO1FBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBVSxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxFQUFFLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFFbkUsQ0FBQztJQUVELGtDQUFTLEdBQVQsVUFBVSxFQUFTO1FBQ2pCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBWSxJQUFJLENBQUMsTUFBTSxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUNwRSxDQUFDO0lBdkNVLGNBQWM7UUFIMUIsaUJBQVUsQ0FBQztZQUNWLFVBQVUsRUFBRSxNQUFNO1NBQ25CLENBQUM7eUNBSzBCLGlCQUFVO09BSnpCLGNBQWMsQ0EyQzFCO0lBQUQscUJBQUM7Q0FBQSxBQTNDRCxJQTJDQztBQTNDWSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IGVudmlyb25tZW50IH0gZnJvbSAnfi9lbnZpcm9ubWVudHMvZW52aXJvbm1lbnQnO1xuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IFByb2R1Y3QgfSBmcm9tICcuLi9lbnRpdHkvcHJvZHVjdCc7XG5pbXBvcnQgeyBDYXRlZ29yeSB9IGZyb20gJy4uL2VudGl0eS9jYXRlZ29yeSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFByb2R1Y3RTZXJ2aWNlIHtcblxuICBhcGlVcmwgPSBlbnZpcm9ubWVudC5zZXJ2ZXJVcmwgKyAnL2FwaS9wcm9kdWN0Lyc7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7IH1cblxuICBmaW5kQWxsKCk6IE9ic2VydmFibGU8UHJvZHVjdFtdPiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQ8UHJvZHVjdFtdPih0aGlzLmFwaVVybCk7XG4gIH1cblxuICAvL3Ryb3V2ZSB1biBwcm9kdWN0XG4gIGZpbmQoaWQ6IG51bWJlcik6IE9ic2VydmFibGU8UHJvZHVjdD4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0PFByb2R1Y3Q+KHRoaXMuYXBpVXJsICsgJ3NpbmdsZS8nICsgaWQpO1xuICB9XG5cbiAgLy90cm91dmUgcHJvZHVjdCBwYXIgcmFwcG9ydCBhIHVzZXJcbiAgZmluZEJ5VXNlcigpOiBPYnNlcnZhYmxlPFByb2R1Y3RbXT4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0PFByb2R1Y3RbXT4odGhpcy5hcGlVcmwgKyAncHJvZHVjdFVzZXJTYWxlJyk7XG4gIH1cblxuICBhZGQocHJvZHVjdDogUHJvZHVjdCk6IE9ic2VydmFibGU8UHJvZHVjdD4ge1xuICAgIGxldCBkYXRhOiBhbnkgPSBPYmplY3QuYXNzaWduKHt9LCBwcm9kdWN0KTtcbiAgICBpZiAocHJvZHVjdC5jYXRlZ29yeSkge1xuICAgICAgZGF0YS5jYXRlZ29yeSA9IHByb2R1Y3QuY2F0ZWdvcnkuaWQ7XG4gICAgfVxuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdDxQcm9kdWN0Pih0aGlzLmFwaVVybCwgZGF0YSk7XG4gIH1cblxuICBkZWxldGUocHJvZHVjdDogUHJvZHVjdCkge1xuICAgIHJldHVybiB0aGlzLmh0dHAuZGVsZXRlKHRoaXMuYXBpVXJsICsgcHJvZHVjdC5pZCk7XG4gIH1cblxuICB1cGRhdGUocHJvZHVjdDogUHJvZHVjdCk6IE9ic2VydmFibGU8UHJvZHVjdD4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAucHV0PFByb2R1Y3Q+KHRoaXMuYXBpVXJsICsgcHJvZHVjdC5pZCwgcHJvZHVjdCk7XG5cbiAgfVxuXG4gIGZpbmRCeUNhdChpZDpudW1iZXIpOiBPYnNlcnZhYmxlPFByb2R1Y3RbXT57XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQ8UHJvZHVjdFtdPih0aGlzLmFwaVVybCArICdjYXRQcm9kdWN0LycgKyBpZCk7XG4gIH1cblxuXG5cbn1cbiJdfQ==