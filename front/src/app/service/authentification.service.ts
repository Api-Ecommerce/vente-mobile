import { Injectable } from '@angular/core';
import * as appSetting from 'application-settings';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { User } from '../entity/user';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class AuthentificationService {
  
  private apiUrl = environment.serverUrl+'/api/user';

  constructor(private http:HttpClient) { }
  addUser(user:User): Observable<User> {
    return this.http.post<User>(this.apiUrl , user);
  }

  // ok

  login(username:string, password:string): Observable<any> {
    return this.http.post<any>(environment.serverUrl+ '/api/login_check', {
      username: username,
      password: password
    }).pipe(
      tap(data => {
        if(data.token) {
          appSetting.setString('token', data.token);
        }
      })
    );
  }
  
  logout() {
    appSetting.remove('token');

  }

   isLogged() {
    return appSetting.hasKey('token');
  }

 
}
