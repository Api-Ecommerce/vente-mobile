"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var authentification_service_1 = require("../service/authentification.service");
var router_1 = require("nativescript-angular/router");
var operators_1 = require("rxjs/operators");
var category_service_1 = require("../service/category.service");
var product_service_1 = require("../service/product.service");
var CategorChoisiComponent = /** @class */ (function () {
    function CategorChoisiComponent(pageRoute, service, userLog, serviceProduct) {
        this.pageRoute = pageRoute;
        this.service = service;
        this.userLog = userLog;
        this.serviceProduct = serviceProduct;
        this.product = [];
    }
    CategorChoisiComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.pageRoute.activatedRoute.pipe(operators_1.switchMap(function (activatedRoute) { return activatedRoute.params; }), operators_1.switchMap(function (params) { return _this.service.find(params['id']); })).subscribe(function (data) {
            _this.serviceProduct.findByCat(data.id).subscribe(function (data) { return _this.product = data; }, function (error) { return console.log(error); }, function () { return console.log(_this.product); });
        }, function (error) { return console.log(error); });
    };
    CategorChoisiComponent.prototype.logged = function () {
        return this.userLog.isLogged();
    };
    CategorChoisiComponent.prototype.logout = function () {
        this.userLog.logout();
    };
    CategorChoisiComponent = __decorate([
        core_1.Component({
            selector: 'ns-categor-choisi',
            templateUrl: './categor-choisi.component.html',
            styleUrls: ['./categor-choisi.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [router_1.PageRoute, category_service_1.CategoryService, authentification_service_1.AuthentificationService, product_service_1.ProductService])
    ], CategorChoisiComponent);
    return CategorChoisiComponent;
}());
exports.CategorChoisiComponent = CategorChoisiComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2F0ZWdvci1jaG9pc2kuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY2F0ZWdvci1jaG9pc2kuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBRWxELGdGQUE4RTtBQUM5RSxzREFBd0Q7QUFDeEQsNENBQTJDO0FBQzNDLGdFQUE4RDtBQUM5RCw4REFBNEQ7QUFVNUQ7SUFLRSxnQ0FBb0IsU0FBb0IsRUFBVSxPQUF3QixFQUFVLE9BQWdDLEVBQVUsY0FBOEI7UUFBeEksY0FBUyxHQUFULFNBQVMsQ0FBVztRQUFVLFlBQU8sR0FBUCxPQUFPLENBQWlCO1FBQVUsWUFBTyxHQUFQLE9BQU8sQ0FBeUI7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFGNUosWUFBTyxHQUFjLEVBQUUsQ0FBQztJQUV3SSxDQUFDO0lBRWpLLHlDQUFRLEdBQVI7UUFBQSxpQkFjQztRQVZDLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksQ0FDaEMscUJBQVMsQ0FBQyxVQUFBLGNBQWMsSUFBSSxPQUFBLGNBQWMsQ0FBQyxNQUFNLEVBQXJCLENBQXFCLENBQUMsRUFDbEQscUJBQVMsQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUEvQixDQUErQixDQUFDLENBQ3JELENBQUMsU0FBUyxDQUNULFVBQUEsSUFBSTtZQUFLLEtBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsT0FBTyxHQUFHLElBQUksRUFBbkIsQ0FBbUIsRUFDbkYsVUFBQyxLQUFLLElBQUssT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFsQixDQUFrQixFQUM3QixjQUFNLE9BQUEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLEVBQXpCLENBQXlCLENBQUMsQ0FBQTtRQUFBLENBQUMsRUFDbkMsVUFBQSxLQUFLLElBQUksT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFsQixDQUFrQixDQUU1QixDQUFDO0lBQ0osQ0FBQztJQUVELHVDQUFNLEdBQU47UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNqQyxDQUFDO0lBR0QsdUNBQU0sR0FBTjtRQUNFLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQTlCVSxzQkFBc0I7UUFObEMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxtQkFBbUI7WUFDN0IsV0FBVyxFQUFFLGlDQUFpQztZQUM5QyxTQUFTLEVBQUUsQ0FBQyxnQ0FBZ0MsQ0FBQztZQUM3QyxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7U0FDcEIsQ0FBQzt5Q0FNK0Isa0JBQVMsRUFBbUIsa0NBQWUsRUFBbUIsa0RBQXVCLEVBQTBCLGdDQUFjO09BTGpKLHNCQUFzQixDQWdDbEM7SUFBRCw2QkFBQztDQUFBLEFBaENELElBZ0NDO0FBaENZLHdEQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDYXRlZ29yeSB9IGZyb20gJy4uL2VudGl0eS9jYXRlZ29yeSc7XG5pbXBvcnQgeyBBdXRoZW50aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2UvYXV0aGVudGlmaWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IFBhZ2VSb3V0ZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBzd2l0Y2hNYXAgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcbmltcG9ydCB7IENhdGVnb3J5U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2UvY2F0ZWdvcnkuc2VydmljZSc7XG5pbXBvcnQgeyBQcm9kdWN0U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2UvcHJvZHVjdC5zZXJ2aWNlJztcbmltcG9ydCB7IFByb2R1Y3QgfSBmcm9tICcuLi9lbnRpdHkvcHJvZHVjdCc7XG5cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbnMtY2F0ZWdvci1jaG9pc2knLFxuICB0ZW1wbGF0ZVVybDogJy4vY2F0ZWdvci1jaG9pc2kuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9jYXRlZ29yLWNob2lzaS5jb21wb25lbnQuY3NzJ10sXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG59KVxuZXhwb3J0IGNsYXNzIENhdGVnb3JDaG9pc2lDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNhdGVnb3J5OiBDYXRlZ29yeTtcbiAgcHJvZHVjdDogUHJvZHVjdFtdID0gW107XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwYWdlUm91dGU6IFBhZ2VSb3V0ZSwgcHJpdmF0ZSBzZXJ2aWNlOiBDYXRlZ29yeVNlcnZpY2UsIHByaXZhdGUgdXNlckxvZzogQXV0aGVudGlmaWNhdGlvblNlcnZpY2UsIHByaXZhdGUgc2VydmljZVByb2R1Y3Q6IFByb2R1Y3RTZXJ2aWNlKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcblxuXG5cbiAgICB0aGlzLnBhZ2VSb3V0ZS5hY3RpdmF0ZWRSb3V0ZS5waXBlKFxuICAgICAgc3dpdGNoTWFwKGFjdGl2YXRlZFJvdXRlID0+IGFjdGl2YXRlZFJvdXRlLnBhcmFtcyksXG4gICAgICBzd2l0Y2hNYXAocGFyYW1zID0+IHRoaXMuc2VydmljZS5maW5kKHBhcmFtc1snaWQnXSkpXG4gICAgKS5zdWJzY3JpYmUoXG4gICAgICBkYXRhID0+IHt0aGlzLnNlcnZpY2VQcm9kdWN0LmZpbmRCeUNhdChkYXRhLmlkKS5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLnByb2R1Y3QgPSBkYXRhLFxuICAgICAgICAoZXJyb3IpID0+IGNvbnNvbGUubG9nKGVycm9yKSxcbiAgICAgICAgKCkgPT4gY29uc29sZS5sb2codGhpcy5wcm9kdWN0KSl9LFxuICAgICAgZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpXG5cbiAgICApO1xuICB9XG5cbiAgbG9nZ2VkKCkge1xuICAgIHJldHVybiB0aGlzLnVzZXJMb2cuaXNMb2dnZWQoKTtcbiAgfVxuXG5cbiAgbG9nb3V0KCkge1xuICAgIHRoaXMudXNlckxvZy5sb2dvdXQoKTtcbiAgfVxuXG59XG4iXX0=