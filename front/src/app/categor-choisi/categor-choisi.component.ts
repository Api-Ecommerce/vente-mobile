import { Component, OnInit } from '@angular/core';
import { Category } from '../entity/category';
import { AuthentificationService } from '../service/authentification.service';
import { PageRoute } from 'nativescript-angular/router';
import { switchMap } from "rxjs/operators";
import { CategoryService } from '../service/category.service';
import { ProductService } from '../service/product.service';
import { Product } from '../entity/product';


@Component({
  selector: 'ns-categor-choisi',
  templateUrl: './categor-choisi.component.html',
  styleUrls: ['./categor-choisi.component.css'],
  moduleId: module.id,
})
export class CategorChoisiComponent implements OnInit {

  category: Category;
  product: Product[] = [];

  constructor(private pageRoute: PageRoute, private service: CategoryService, private userLog: AuthentificationService, private serviceProduct: ProductService) { }

  ngOnInit() {



    this.pageRoute.activatedRoute.pipe(
      switchMap(activatedRoute => activatedRoute.params),
      switchMap(params => this.service.find(params['id']))
    ).subscribe(
      data => {this.serviceProduct.findByCat(data.id).subscribe(data => this.product = data,
        (error) => console.log(error),
        () => console.log(this.product))},
      error => console.log(error)

    );
  }

  logged() {
    return this.userLog.isLogged();
  }


  logout() {
    this.userLog.logout();
  }

}
