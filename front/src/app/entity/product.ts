import { Category } from "./category";

export interface Product {
    id?:number;
    name:string;
    price:number;
    category?:Category;
    description:string;
    img:string;
    size:string;
}
