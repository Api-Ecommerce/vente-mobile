import { Product } from "./product";

export interface User {
    id?:number;
    username:string;
    email:string;
    password:string;
    productSales?:Product[];
    productPurchases?:Product[];
}
