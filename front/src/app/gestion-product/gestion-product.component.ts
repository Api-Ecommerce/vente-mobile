import { Component, OnInit } from '@angular/core';


import { User } from '../entity/user';
import { UserService } from '../service/user.service';
import { ProductService } from '../service/product.service';
import { Product } from '../entity/product';
import { AuthentificationService } from '../service/authentification.service';



@Component({
  selector: 'ns-gestion-product',
  templateUrl: './gestion-product.component.html',
  styleUrls: ['./gestion-product.component.css'],
  moduleId: module.id,
})
export class GestionProductComponent implements OnInit {

  user:User;
  products:Product[] = [];

  constructor(private serviceUser:UserService, private productserv:ProductService ,private userLog:AuthentificationService) { }

  ngOnInit() {

    //trouve le user connecter
    this.serviceUser.findByUser().subscribe( data => this.user = data);
    this.productserv.findByUser().subscribe(data => this.products = data);



  }
  
  deleteProduct(product:Product){
    this.productserv.delete(product).subscribe(() => this.ngOnInit());
    this.ngOnInit();
  }



  logged() {
    return this.userLog.isLogged();
  }


  logout(){
    this.userLog.logout();
  }




}
