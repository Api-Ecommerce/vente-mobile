"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("../service/user.service");
var product_service_1 = require("../service/product.service");
var authentification_service_1 = require("../service/authentification.service");
var GestionProductComponent = /** @class */ (function () {
    function GestionProductComponent(serviceUser, productserv, userLog) {
        this.serviceUser = serviceUser;
        this.productserv = productserv;
        this.userLog = userLog;
        this.products = [];
    }
    GestionProductComponent.prototype.ngOnInit = function () {
        var _this = this;
        //trouve le user connecter
        this.serviceUser.findByUser().subscribe(function (data) { return _this.user = data; });
        this.productserv.findByUser().subscribe(function (data) { return _this.products = data; });
    };
    GestionProductComponent.prototype.deleteProduct = function (product) {
        var _this = this;
        this.productserv.delete(product).subscribe(function () { return _this.ngOnInit(); });
        this.ngOnInit();
    };
    GestionProductComponent.prototype.logged = function () {
        return this.userLog.isLogged();
    };
    GestionProductComponent.prototype.logout = function () {
        this.userLog.logout();
    };
    GestionProductComponent = __decorate([
        core_1.Component({
            selector: 'ns-gestion-product',
            templateUrl: './gestion-product.component.html',
            styleUrls: ['./gestion-product.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [user_service_1.UserService, product_service_1.ProductService, authentification_service_1.AuthentificationService])
    ], GestionProductComponent);
    return GestionProductComponent;
}());
exports.GestionProductComponent = GestionProductComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2VzdGlvbi1wcm9kdWN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImdlc3Rpb24tcHJvZHVjdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFJbEQsd0RBQXNEO0FBQ3RELDhEQUE0RDtBQUU1RCxnRkFBOEU7QUFVOUU7SUFLRSxpQ0FBb0IsV0FBdUIsRUFBVSxXQUEwQixFQUFVLE9BQStCO1FBQXBHLGdCQUFXLEdBQVgsV0FBVyxDQUFZO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWU7UUFBVSxZQUFPLEdBQVAsT0FBTyxDQUF3QjtRQUZ4SCxhQUFRLEdBQWEsRUFBRSxDQUFDO0lBRW9HLENBQUM7SUFFN0gsMENBQVEsR0FBUjtRQUFBLGlCQVFDO1FBTkMsMEJBQTBCO1FBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUMsU0FBUyxDQUFFLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLEVBQWhCLENBQWdCLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxFQUFwQixDQUFvQixDQUFDLENBQUM7SUFJeEUsQ0FBQztJQUVELCtDQUFhLEdBQWIsVUFBYyxPQUFlO1FBQTdCLGlCQUdDO1FBRkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsU0FBUyxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsUUFBUSxFQUFFLEVBQWYsQ0FBZSxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUFJRCx3Q0FBTSxHQUFOO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDakMsQ0FBQztJQUdELHdDQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUEvQlUsdUJBQXVCO1FBTm5DLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLFdBQVcsRUFBRSxrQ0FBa0M7WUFDL0MsU0FBUyxFQUFFLENBQUMsaUNBQWlDLENBQUM7WUFDOUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1NBQ3BCLENBQUM7eUNBTWdDLDBCQUFXLEVBQXNCLGdDQUFjLEVBQWtCLGtEQUF1QjtPQUw3Ryx1QkFBdUIsQ0FvQ25DO0lBQUQsOEJBQUM7Q0FBQSxBQXBDRCxJQW9DQztBQXBDWSwwREFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5cbmltcG9ydCB7IFVzZXIgfSBmcm9tICcuLi9lbnRpdHkvdXNlcic7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2UvdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IFByb2R1Y3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9wcm9kdWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgUHJvZHVjdCB9IGZyb20gJy4uL2VudGl0eS9wcm9kdWN0JztcbmltcG9ydCB7IEF1dGhlbnRpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9hdXRoZW50aWZpY2F0aW9uLnNlcnZpY2UnO1xuXG5cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbnMtZ2VzdGlvbi1wcm9kdWN0JyxcbiAgdGVtcGxhdGVVcmw6ICcuL2dlc3Rpb24tcHJvZHVjdC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2dlc3Rpb24tcHJvZHVjdC5jb21wb25lbnQuY3NzJ10sXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG59KVxuZXhwb3J0IGNsYXNzIEdlc3Rpb25Qcm9kdWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICB1c2VyOlVzZXI7XG4gIHByb2R1Y3RzOlByb2R1Y3RbXSA9IFtdO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgc2VydmljZVVzZXI6VXNlclNlcnZpY2UsIHByaXZhdGUgcHJvZHVjdHNlcnY6UHJvZHVjdFNlcnZpY2UgLHByaXZhdGUgdXNlckxvZzpBdXRoZW50aWZpY2F0aW9uU2VydmljZSkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG5cbiAgICAvL3Ryb3V2ZSBsZSB1c2VyIGNvbm5lY3RlclxuICAgIHRoaXMuc2VydmljZVVzZXIuZmluZEJ5VXNlcigpLnN1YnNjcmliZSggZGF0YSA9PiB0aGlzLnVzZXIgPSBkYXRhKTtcbiAgICB0aGlzLnByb2R1Y3RzZXJ2LmZpbmRCeVVzZXIoKS5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLnByb2R1Y3RzID0gZGF0YSk7XG5cblxuXG4gIH1cbiAgXG4gIGRlbGV0ZVByb2R1Y3QocHJvZHVjdDpQcm9kdWN0KXtcbiAgICB0aGlzLnByb2R1Y3RzZXJ2LmRlbGV0ZShwcm9kdWN0KS5zdWJzY3JpYmUoKCkgPT4gdGhpcy5uZ09uSW5pdCgpKTtcbiAgICB0aGlzLm5nT25Jbml0KCk7XG4gIH1cblxuXG5cbiAgbG9nZ2VkKCkge1xuICAgIHJldHVybiB0aGlzLnVzZXJMb2cuaXNMb2dnZWQoKTtcbiAgfVxuXG5cbiAgbG9nb3V0KCl7XG4gICAgdGhpcy51c2VyTG9nLmxvZ291dCgpO1xuICB9XG5cblxuXG5cbn1cbiJdfQ==