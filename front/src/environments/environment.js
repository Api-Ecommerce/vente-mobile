"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * L'ip en 10.0.2.2 pointe sur le localhost de la machine depuis
 * un émulateur. L'émulateur n'a pas accès directement à localhost
 */
exports.environment = {
    production: false,
    serverUrl: 'http://192.168.1.167:8080'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW52aXJvbm1lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJlbnZpcm9ubWVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBOzs7R0FHRztBQUNVLFFBQUEsV0FBVyxHQUFHO0lBQ3pCLFVBQVUsRUFBRSxLQUFLO0lBQ2pCLFNBQVMsRUFBRSwyQkFBMkI7Q0FDdkMsQ0FBQztBQUVGOzs7Ozs7R0FNRztBQUNILG1FQUFtRSIsInNvdXJjZXNDb250ZW50IjpbIlxuLyoqXG4gKiBMJ2lwIGVuIDEwLjAuMi4yIHBvaW50ZSBzdXIgbGUgbG9jYWxob3N0IGRlIGxhIG1hY2hpbmUgZGVwdWlzXG4gKiB1biDDqW11bGF0ZXVyLiBMJ8OpbXVsYXRldXIgbidhIHBhcyBhY2PDqHMgZGlyZWN0ZW1lbnQgw6AgbG9jYWxob3N0XG4gKi9cbmV4cG9ydCBjb25zdCBlbnZpcm9ubWVudCA9IHtcbiAgcHJvZHVjdGlvbjogZmFsc2UsXG4gIHNlcnZlclVybDogJ2h0dHA6Ly8xOTIuMTY4LjEuMTY3OjgwODAnXG59O1xuXG4vKlxuICogRm9yIGVhc2llciBkZWJ1Z2dpbmcgaW4gZGV2ZWxvcG1lbnQgbW9kZSwgeW91IGNhbiBpbXBvcnQgdGhlIGZvbGxvd2luZyBmaWxlXG4gKiB0byBpZ25vcmUgem9uZSByZWxhdGVkIGVycm9yIHN0YWNrIGZyYW1lcyBzdWNoIGFzIGB6b25lLnJ1bmAsIGB6b25lRGVsZWdhdGUuaW52b2tlVGFza2AuXG4gKlxuICogVGhpcyBpbXBvcnQgc2hvdWxkIGJlIGNvbW1lbnRlZCBvdXQgaW4gcHJvZHVjdGlvbiBtb2RlIGJlY2F1c2UgaXQgd2lsbCBoYXZlIGEgbmVnYXRpdmUgaW1wYWN0XG4gKiBvbiBwZXJmb3JtYW5jZSBpZiBhbiBlcnJvciBpcyB0aHJvd24uXG4gKi9cbi8vIGltcG9ydCAnem9uZS5qcy9kaXN0L3pvbmUtZXJyb3InOyAgLy8gSW5jbHVkZWQgd2l0aCBBbmd1bGFyIENMSS5cbiJdfQ==