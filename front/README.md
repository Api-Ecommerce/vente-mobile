## Cloner le projet
1. Faire un git clone
2. Aller dans le dossier obtenu et faire un `npm install`
3. Lancer le projet avec `tns run android`
Si vous lancez l'appli directement sur un téléphone et pas sur l'émulateur, ne pas oublier de changer les url du webservice rest pour les passer de http://10.0.2.2:8080 à http://192.168.1.xxx selon votre ip locale (que vous pouvez obtenir en faisant un `hostname -I` dans un terminal)


## I. Authentification
1. Dans le app.module, rajouter dans les imports le NativeScriptHttpClientModule
2. Créer un service authentication avec ng g s
3. Dans ce service, faire une méthode addUser(user:User) et une méthode login(email:string, password:string)
4. Y injécter le HttpClient classique comme on fait d'habitude avec Angular
5. Faire que la méthode addUser fasse un post vers http://10.0.2.2:8080/api/user/
6. Faire que la méthode login fasse un post vers http://10.0.2.2:8080/api/login_check
7. Dans le service rajouter un import * as appSetting from 'application-settings'
8. Dans la méthode login, rajouter un pipe sur le login et faire que dans le tap, on mette le token renvoyé par le login dans appSetting.setString('token', leToken)
9. Injecter le service dans le LoginComponent et faire que le register et le login lancent les méthodes qui leur correspondent
10. Faire que le register ne se lance que si les deux mots de passe correspondent et éventuellement rajouter un petit text de feedback

## II. Gestion chiens
1. En partant de l'application nativescript-introduction ( https://gitlab.com/simplonlyon/P6/nativescript-introduction )
2. Créer une nouvelle page/component my-dogs, lui faire une route dans le routing module et rajouter un lien vers cette page à côté du bouton logout (dans la page exo-layout)
3. Faire une entity dog et un service dog avec les méthodes findAll, findByUser et add qui feront appel aux méthodes symfony qu'on a fait hier
4. Récupérer l'intercepteur ( https://gitlab.com/simplonlyon/P6/angular-contact/blob/master/src/app/service/interceptor/authorization.service.ts ), l'adapter pour qu'il n'utilise pas le localStorage mais plutôt l'application-settings, puis l'inscrire dans le app.module
5. Dans le my-dog component, injecter le service dog et récupérer les chiens, les afficher dans le template en utilisant un ListView (voir la doc)
6. Créer un component/page add-dog, lui faire une route et rajouter un lien dans l'AppBar du my-dog
7. Dans ce component, rajouter une propriété typée dog et l'initialiser, ajouter également une methode add() qui utilise le service dog pour faire persister le dog
8. Dans le template faire un "formulaire" pour le chien avec des ngModel
